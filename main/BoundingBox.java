package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

public class BoundingBox extends JPanel {
	
	double x1;
	double x2;
	double y1;
	double y2;
	
	public BoundingBox() {
		this.setOpaque(false);
		this.setBounds((int) 0, (int) 0,(int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM));
		this.setVisible(false);
	}
	
	public void setCoordinates(double x1, double x2, double y1, double y2) {
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		
		if (this.x1<0){
			this.x1=0;
		}
		if (this.x2>this.getWidth()){
			this.x2=this.getWidth()-1;
		}
		
		if (this.y1<0){
			this.y1=0;
		}
		if (this.y2>this.getHeight()){
			this.y2=this.getHeight()-1;
		}
	}
	
	public void paint(Graphics g){
		//super.paint(g);
		g.setColor(Color.BLUE);
		Graphics2D g2 = (Graphics2D) g;
		g2.draw(new Rectangle2D.Double(x1, y1, x2-x1, y2-y1));
	}
}