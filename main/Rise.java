package main;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.UIManager;
import java.awt.Font;

import com.softsynth.jsyn.ExponentialLag;
import com.softsynth.jsyn.MultiplyUnit;
import com.softsynth.jsyn.SynthMixer;

public class Rise {
	
	static int PAGE_WIDTH = 1224;
	static int PAGE_HEIGHT = 792;
	static int PAGE_MARGIN = 36+18;
	static int IMAGE_WIDTH = PAGE_WIDTH-PAGE_MARGIN*2;
	static int IMAGE_HEIGHT = PAGE_HEIGHT-PAGE_MARGIN*2;
	static double ZOOM = .64;
	static float FONT = 12;
	static float LINE_THICKNESS = (float) .05;
	static int FIRST_NUM = 16;
	static int VOICES = 12;
	static int X_DIV = 1;
	static int Y_DIV = 1;
	static int CURRENT_X_PAGE = 0;
	static int CURRENT_Y_PAGE = 0;
	//static float TOTAL_TIME = (VOICES-1)*60;
	static float TRANSPOSE_SCORE = 36;
	static float TRANSPOSE_SOUND = 0;
	static int MODE = 0;
	static double MASTER_AMP = 0;
	static double START_AMP = 0;
	static double END_AMP = 0;
	static double MIN_SPECTRUM = 0;
	static double MAX_SPECTRUM = 0;
	static int START_DENSITY = 0;
	static int END_DENSITY = 0;
	static double START_DUR = 0;
	static double END_DUR = 0;
	static double TOTAL_DUR = 1650;
	static double START_TIME = 0;
	static double FADE_DUR = 150;
	static boolean IS_ENGINE_ON = false;
	static int SCROLLBAR_WIDTH;
	
	static CuePanel CUE_PANEL;
	
	Score score;
	JScrollPane scroller;
	JPanel scrollPanel;
	
	static NoteFrame NOTE_FRAME = new NoteFrame();
	static BoundingBox BOUNDING_BOX = new BoundingBox();
	static ScrollBar SCROLLBAR = new ScrollBar();
	
	static SynthMixer MASTER_MIXER;
	static ExponentialLag MASTER_FADER;
	static MultiplyUnit MASTER_MULT;
	static SynthMixer CLICK_MIXER;
	static ExponentialLag CLICK_FADER;
	static MultiplyUnit CLICK_MULT;
	
	static double GRAIN_AMP_VAL_START;
	static double GRAIN_AMP_VAL_END;
	static double GRAIN_AMP_JIT_START;
	static double GRAIN_AMP_JIT_END;
	
	static double GRAIN_SOUND_DUR_VAL_START;
	static double GRAIN_SOUND_DUR_VAL_END;
	static double GRAIN_SOUND_DUR_JIT_START;
	static double GRAIN_SOUND_DUR_JIT_END;
	
	static double GRAIN_SILENCE_DUR_VAL_START;
	static double GRAIN_SILENCE_DUR_VAL_END;
	static double GRAIN_SILENCE_DUR_JIT_START;
	static double GRAIN_SILENCE_DUR_JIT_END;
	
	static double GLISS_AMP_START;
	static double GLISS_AMP_END;

	public static void setUIFont (javax.swing.plaf.FontUIResource f){
	    java.util.Enumeration keys = UIManager.getDefaults().keys();
	    while (keys.hasMoreElements()) {
	      Object key = keys.nextElement();
	      Object value = UIManager.get (key);
	      if (value instanceof javax.swing.plaf.FontUIResource)
	        UIManager.put (key, f);
	      }
	    } 

	
	public Rise() {
		
		setUIFont (new javax.swing.plaf.FontUIResource("Liberation Serif", Font.BOLD, 8));

		JFrame scoreFrame = new JFrame();
		scoreFrame.setTitle("Score Frame");
		Container scoreContainer = scoreFrame.getContentPane();
		
		scrollPanel = new JPanel();
		scroller = new JScrollPane(scrollPanel,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		score = new Score();
		score.setPreferredSize(new Dimension((int) (PAGE_WIDTH*ZOOM), (int) (PAGE_HEIGHT*ZOOM)));
		score.setBackground(Color.WHITE);
		scrollPanel.add(score);
		scoreContainer.add(scroller);
		scroller.getViewport().setScrollMode(JViewport.BLIT_SCROLL_MODE);
		
		ToolFrame controlFrame = new ToolFrame(score);
		
		scoreFrame.pack();
		scoreFrame.setSize(800,550);
		scoreFrame.setVisible(true);
		
		controlFrame.setLocation(800, 0);
		controlFrame.setVisible(true);
	}
	
	public static void main(String args[]) {
		Rise rise = new Rise();
	}
}
