package main;

import java.awt.Container;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.Rectangle;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JSlider;
import java.awt.Dimension;
import java.util.Hashtable;

import com.softsynth.jsyn.Synth;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ToolFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JTabbedPane jTabbedPane = null;
	private JPanel editModePanel = null;
	private JButton zoomInButtonEdit = null;
	private JButton zoomOutButtonEdit = null;
	private JButton zoomInButtonPlay = null;
	private JButton zoomOutButtonPlay = null;
	private JTextField yGridTextField = null;
	private JTextField xGridTextField = null;
	private JLabel gridLabel = null;
	private JLabel xDivLabel = null;
	private JLabel divLabel1 = null;
	private JLabel divLabel2 = null;
	private JLabel yDivLabel = null;
	private JPanel pageLayoutPanel = null;
	private JTextField yDimTextField = null;
	private JTextField xDimTextField = null;
	private JLabel pageLabel = null;
	private JLabel pageWidthLabel = null;
	private JLabel pageHeightLabel = null;
	private JLabel pageMarginLabel = null;
	private JTextField marginTextField = null;
	private JPanel pieceVariablesPanel = null;
	private JTextField numeratorTextField = null;
	private JTextField sectionsTextField = null;
	private JLabel pieceVariablesLabel = null;
	private JLabel numeratorLabel = null;
	private JLabel sectionsLabel = null;
	private JLabel transpositionScoreLabel = null;
	private JTextField transpositionScoreTextField = null;
	private JPanel formatPanel = null;
	private JTextField fontSizeTextField = null;
	private JTextField lineThicknessTextField = null;
	private JLabel formatLabel = null;
	private JLabel fontLabel = null;
	private JLabel lineLabel = null;
	private JButton xPageDownEdit = null;
	private JButton xPageUpEdit = null;
	private JButton yPageUpEdit = null;
	private JButton yPageDownEdit = null;
	private JButton xPageDownPlay = null;
	private JButton xPageUpPlay = null;
	private JButton yPageUpPlay = null;
	private JButton yPageDownPlay = null;
	private JLabel pageNavEditLabel = null;
	private JLabel zoomEditLabel = null;
	private JLabel pageNavPlayLabel = null;
	private JLabel zoomPlayLabel = null;
	private JButton redrawButton = null;
	private JPanel playModePanel = null;
	private JSlider masterAmpSlider = null;
	private JButton saveButton = null;
	private JButton openButton = null;
	private JButton printButton = null;
	private JLabel masterLabel = null;
	private JLabel pieceDurLabel = null;
	private JTextField minTextField = null;
	private JLabel colonLabel = null;
	private JTextField secTextField = null;
	private JLabel minsecLabel = null;
	private double glissAmp = 0;
	private double masterAmp = 1;
	private double grainAmp = 0;
	private double clickAmp = 1;
	
	Score score;
	private JButton playButton = null;
	private JButton pauseButton = null;
	private JLabel transpositionSoundingLabel = null;
	private JTextField transpositionSoundingTextField = null;
	JFileChooser fc=new JFileChooser();
	PrintUtilities printUtilities = new PrintUtilities();
	
	WaveForm clickAndPlay;
	Glissandi gliss;  //  @jve:decl-index=0:
	GrainStream[] grainStream;
	//Cue cue;
	private JLabel ampLabel = null;
	private JTabbedPane jTabbedPane1 = null;
	private JPanel masterControlsPanel = null;
	private JPanel grainValuesPanel = null;
	private JPanel glissValuesPanel = null;
	private JPanel spectrumPanel = null;
	private JTabbedPane spectrumTabPane = null;
	private JPanel spectrumValPanel = null;
	private JSlider spectrumMaxValFader = null;
	private JSlider spectrumMinValFader = null;
	private JLabel spectrumLabel = null;
	private JTabbedPane toolsTabbedPane = null;
	private JLabel minMaxSpectrumValLabel = null;
	private JPanel glissAmpPanel = null;
	private JPanel glissAmpValPanel = null;
	private JSlider glissAmpValMaxFader = null;
	private JSlider glissAmpValMinFader = null;
	private JLabel glissAmpLabel = null;
	private JLabel startEndGlissampValLabel = null;
	
	private JPanel grainAmpPanel = null;
	private JTabbedPane grainAmpTabPane = null;
	private JPanel grainAmpValPanel = null;
	private JSlider grainAmpValMaxFader = null;
	private JSlider grainAmpValMinFader = null;
	private JLabel grainAmpLabel = null;
	private JPanel grainAmpJitPanel = null;
	private JSlider grainAmpJitMaxFader = null;
	private JSlider grainAmpJitMinFader = null;
	private JLabel startEndGrainampValLabel = null;
	private JLabel startEndGrainAmpJitLabel = null;
	
	private JPanel grainDenPanel = null;
	private JTabbedPane grainDenTabPane = null;
	private JPanel grainDenValPanel = null;
	private JSlider grainDenValMaxFader = null;
	private JSlider grainDenValMinFader = null;
	private JLabel grainDenLabel = null;
	
	private JPanel grainSoundDurPanel = null;
	private JTabbedPane grainSoundDurTabPane = null;
	private JPanel grainSoundDurValPanel = null;
	private JSlider grainSoundDurValMaxFader = null;
	private JSlider grainSoundDurValMinFader = null;
	private JLabel grainSoundDurLabel = null;
	private JPanel grainSoundDurJitPanel = null;
	private JSlider grainSoundDurJitMaxFader = null;
	private JSlider grainSoundDurJitMinFader = null;
	private JLabel startEndGrainSoundDurJitLabel = null;
	
	private JPanel grainSilenceDurPanel = null;
	private JTabbedPane grainSilenceDurTabPane = null;
	private JPanel grainSilenceDurValPanel = null;
	private JSlider grainSilenceDurValMaxFader = null;
	private JSlider grainSilenceDurValMinFader = null;
	private JLabel grainSilenceDurLabel = null;
	private JPanel grainSilenceDurJitPanel = null;
	private JSlider grainSilenceDurJitMaxFader = null;
	private JSlider grainSilenceDurJitMinFader = null;
	private JLabel startEndGrainSilenceDurJitLabel = null;
	private JPanel masterFaderPanel = null;
	private JPanel glissFaderPanel = null;
	private JSlider glissFader = null;
	private JLabel glissFaderLabel = null;
	private JLabel glissFaderLabel2 = null;
	private JPanel grainsFaderPanel = null;
	private JSlider grainFader = null;
	private JLabel grainFaderLabel = null;
	private JLabel grainFaderLabel2 = null;
	private JPanel timePanel = null;
	private JPanel startTimePanel = null;
	private JLabel startTimeLabel = null;
	private JLabel minsecStartLabel = null;
	private JTextField minStartTextField = null;
	private JLabel colonStartLabel = null;
	private JTextField secStartTextField = null;
	private JPanel fadePanel = null;
	private JLabel fadeOutLabe = null;
	private JLabel minsecFadeLabel = null;
	private JTextField minFadeField = null;
	private JLabel colonStartLabel1 = null;
	private JTextField secFadeField = null;
	PitchVector pV;
	private JPanel cuePanel = null;
	private JPanel clickFaderPanel = null;
	private JSlider clickFader = null;
	private JLabel clickFaderLabel = null;
	private JLabel grainFaderLabel21 = null;
	/**
	 * This is the default constructor
	 */
	public ToolFrame(Score s) {
		super();
		score = s;
		initialize();
		Synth.startEngine(0);
		gliss = new Glissandi();
		gliss.start();
		
//		grainStream = new GrainStream[40];
//		for (int i = 0; i < 40; i++){
//			grainStream[i] = new GrainStream();
//			grainStream[i].start();
//		}
		
		//cue = new Cue(cuePanel);
		//cue.start();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(517, 522);
		this.setContentPane(getJContentPane());
		this.setTitle("Tool Frame");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(getJTabbedPane(), null);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jTabbedPane	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getJTabbedPane() {
		if (jTabbedPane == null) {
			jTabbedPane = new JTabbedPane();
			jTabbedPane.setBounds(new Rectangle(0, 0, 513, 522));
			jTabbedPane.addTab("Edit Mode", null, getEditModePanel(), null);
			jTabbedPane.addTab("Play Mode", null, getPlayModePanel(), null);
			jTabbedPane.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					Rise.MODE = jTabbedPane.getSelectedIndex();
					if (Rise.MODE == 0){
						Rise.SCROLLBAR.setVisible(false);
					}
					else {
						Rise.SCROLLBAR.setVisible(true);
					}
				}
			});
		}
		return jTabbedPane;
	}

	/**
	 * This method initializes editModePanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getEditModePanel() {
		if (editModePanel == null) {
			zoomEditLabel = new JLabel();
			zoomEditLabel.setBounds(new Rectangle(192, 41, 36, 16));
			zoomEditLabel.setText("Zoom");
			pageNavEditLabel = new JLabel();
			pageNavEditLabel.setBounds(new Rectangle(25, 15, 101, 16));
			pageNavEditLabel.setText("Page Navigation");
			editModePanel = new JPanel();
			editModePanel.setLayout(null);
			editModePanel.add(getZoomInButtonEdit(), null);
			editModePanel.add(getZoomOutButtonEdit(), null);
			editModePanel.add(getXPageDownEdit(), null);
			editModePanel.add(getXPageUpEdit(), null);
			editModePanel.add(getYPageUpEdit(), null);
			editModePanel.add(getYPageDownEdit(), null);
			editModePanel.add(pageNavEditLabel, null);
			editModePanel.add(zoomEditLabel, null);
			editModePanel.add(getRedrawButton(), null);
			editModePanel.add(getSaveButton(), null);
			editModePanel.add(getOpenButton(), null);
			editModePanel.add(getPrintButton(), null);
			editModePanel.add(getToolsTabbedPane(), null);
		}
		return editModePanel;
	}

	/**
	 * This method initializes zoomInButtonEdit	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getZoomInButtonEdit() {
		if (zoomInButtonEdit == null) {
			zoomInButtonEdit = new JButton();
			zoomInButtonEdit.setBounds(new Rectangle(210, 60, 41, 26));
			zoomInButtonEdit.setText("+");
			zoomInButtonEdit.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.ZOOM = Rise.ZOOM*1.25;
					Rise.BOUNDING_BOX.setBounds((int) 0, (int) 0,(int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM));
					Rise.SCROLLBAR.setBounds((int) 0, (int) 0,(int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM));
					score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
					
					Rise.BOUNDING_BOX.x1 = Rise.BOUNDING_BOX.x1*1.25;
					Rise.BOUNDING_BOX.x2 = Rise.BOUNDING_BOX.x2*1.25;
					Rise.BOUNDING_BOX.y1 = Rise.BOUNDING_BOX.y1*1.25;
					Rise.BOUNDING_BOX.y2 = Rise.BOUNDING_BOX.y2*1.25;
					Rise.BOUNDING_BOX.repaint();
					
					score.repaint();
					score.revalidate();
				}
			});
		}
		return zoomInButtonEdit;
	}

	/**
	 * This method initializes zoomOutButtonEdit	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getZoomOutButtonEdit() {
		if (zoomOutButtonEdit == null) {
			zoomOutButtonEdit = new JButton();
			zoomOutButtonEdit.setBounds(new Rectangle(170, 60, 41, 26));
			zoomOutButtonEdit.setText("-");
			zoomOutButtonEdit.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.ZOOM = Rise.ZOOM*(1/1.25);
					Rise.BOUNDING_BOX.setBounds((int) 0, (int) 0,(int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM));
					Rise.SCROLLBAR.setBounds((int) 0, (int) 0,(int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM));
					score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
					
					Rise.BOUNDING_BOX.x1 = Rise.BOUNDING_BOX.x1*(1/1.25);
					Rise.BOUNDING_BOX.x2 = Rise.BOUNDING_BOX.x2*(1/1.25);
					Rise.BOUNDING_BOX.y1 = Rise.BOUNDING_BOX.y1*(1/1.25);
					Rise.BOUNDING_BOX.y2 = Rise.BOUNDING_BOX.y2*(1/1.25);
					Rise.BOUNDING_BOX.repaint();
					
					score.repaint();
					score.revalidate();
					}
			});
		}
		return zoomOutButtonEdit;
	}
	
	/**
	 * This method initializes zoomInButtonPlay	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getZoomInButtonPlay() {
		if (zoomInButtonPlay == null) {
			zoomInButtonPlay = new JButton();
			zoomInButtonPlay.setBounds(new Rectangle(254, 48, 41, 26));
			zoomInButtonPlay.setText("+");
			zoomInButtonPlay.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.ZOOM = Rise.ZOOM*1.25;
					Rise.BOUNDING_BOX.setBounds((int) 0, (int) 0,(int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM));
					Rise.SCROLLBAR.setBounds((int) 0, (int) 0,(int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM));
					score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
					
					Rise.BOUNDING_BOX.x1 = Rise.BOUNDING_BOX.x1*1.25;
					Rise.BOUNDING_BOX.x2 = Rise.BOUNDING_BOX.x2*1.25;
					Rise.BOUNDING_BOX.y1 = Rise.BOUNDING_BOX.y1*1.25;
					Rise.BOUNDING_BOX.y2 = Rise.BOUNDING_BOX.y2*1.25;
					Rise.BOUNDING_BOX.repaint();
					
					score.repaint();
					score.revalidate();
				}
			});
		}
		return zoomInButtonPlay;
	}

	/**
	 * This method initializes zoomOutButtonPlay	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getZoomOutButtonPlay() {
		if (zoomOutButtonPlay == null) {
			zoomOutButtonPlay = new JButton();
			zoomOutButtonPlay.setBounds(new Rectangle(214, 48, 41, 26));
			zoomOutButtonPlay.setText("-");
			zoomOutButtonPlay.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.ZOOM = Rise.ZOOM*(1/1.25);
					Rise.BOUNDING_BOX.setBounds((int) 0, (int) 0,(int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM));
					Rise.SCROLLBAR.setBounds((int) 0, (int) 0,(int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM));
					score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
					
					Rise.BOUNDING_BOX.x1 = Rise.BOUNDING_BOX.x1*(1/1.25);
					Rise.BOUNDING_BOX.x2 = Rise.BOUNDING_BOX.x2*(1/1.25);
					Rise.BOUNDING_BOX.y1 = Rise.BOUNDING_BOX.y1*(1/1.25);
					Rise.BOUNDING_BOX.y2 = Rise.BOUNDING_BOX.y2*(1/1.25);
					Rise.BOUNDING_BOX.repaint();
					
					score.repaint();
					score.revalidate();
					}
			});
		}
		return zoomOutButtonPlay;
	}

	/**
	 * This method initializes yGridTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getYGridTextField() {
		if (yGridTextField == null) {
			yGridTextField = new JTextField();
			yGridTextField.setText("1");
			yGridTextField.setBounds(new Rectangle(330, 70, 21, 22));
			yGridTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.BOUNDING_BOX.setVisible(false);
					Rise.NOTE_FRAME.setVisible(false);
					Rise.X_DIV = Integer.valueOf(xGridTextField.getText()).intValue();
					Rise.Y_DIV = Integer.valueOf(yGridTextField.getText()).intValue();
					Rise.CURRENT_X_PAGE = 0;
					Rise.CURRENT_Y_PAGE = 0;
					score.removeAll();
					score.add(Rise.BOUNDING_BOX);
					score.add(Rise.SCROLLBAR);
					score.setNotes();
					score.setNoteChoices();
					score.repaint();
					score.revalidate();
				}
			});
		}
		return yGridTextField;
	}

	/**
	 * This method initializes xGridTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getXGridTextField() {
		if (xGridTextField == null) {
			xGridTextField = new JTextField();
			xGridTextField.setText("1");
			xGridTextField.setBounds(new Rectangle(240, 70, 21, 22));
			xGridTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.BOUNDING_BOX.setVisible(false);
					Rise.NOTE_FRAME.setVisible(false);
					Rise.X_DIV = Integer.valueOf(xGridTextField.getText()).intValue();
					Rise.Y_DIV = Integer.valueOf(yGridTextField.getText()).intValue();
					Rise.CURRENT_X_PAGE = 0;
					Rise.CURRENT_Y_PAGE = 0;
					score.removeAll();
					score.add(Rise.BOUNDING_BOX);
					score.add(Rise.SCROLLBAR);
					score.setNotes();
					score.setNoteChoices();
					score.repaint();
					score.revalidate();
				}
			});
		}
		return xGridTextField;
	}

	/**
	 * This method initializes pageLayoutPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPageLayoutPanel() {
		if (pageLayoutPanel == null) {
			yDivLabel = new JLabel();
			yDivLabel.setText("Vertical");
			yDivLabel.setBounds(new Rectangle(330, 30, 47, 16));
			divLabel2 = new JLabel();
			divLabel2.setText("Divisions");
			divLabel2.setBounds(new Rectangle(240, 50, 59, 16));
			divLabel1 = new JLabel();
			divLabel1.setText("Divisions");
			divLabel1.setBounds(new Rectangle(330, 50, 59, 16));
			xDivLabel = new JLabel();
			xDivLabel.setText("Horizontal");
			xDivLabel.setBounds(new Rectangle(240, 30, 66, 16));
			gridLabel = new JLabel();
			gridLabel.setText("Tile Score");
			gridLabel.setBounds(new Rectangle(240, 10, 61, 16));
			pageMarginLabel = new JLabel();
			pageMarginLabel.setBounds(new Rectangle(142, 40, 50, 16));
			pageMarginLabel.setText("Margins");
			pageHeightLabel = new JLabel();
			pageHeightLabel.setBounds(new Rectangle(82, 40, 42, 16));
			pageHeightLabel.setText("Height");
			pageWidthLabel = new JLabel();
			pageWidthLabel.setBounds(new Rectangle(22, 40, 36, 16));
			pageWidthLabel.setText("Width");
			pageLabel = new JLabel();
			pageLabel.setBounds(new Rectangle(20, 10, 144, 16));
			pageLabel.setText("Page Layout (in Inches)");
			pageLayoutPanel = new JPanel();
			pageLayoutPanel.setLayout(null);
			pageLayoutPanel.add(getYDimTextField(), null);
			pageLayoutPanel.add(getXDimTextField(), null);
			pageLayoutPanel.add(pageLabel, null);
			pageLayoutPanel.add(pageWidthLabel, null);
			pageLayoutPanel.add(pageHeightLabel, null);
			pageLayoutPanel.add(pageMarginLabel, null);
			pageLayoutPanel.add(getMarginTextField(), null);
			pageLayoutPanel.add(gridLabel, null);
			pageLayoutPanel.add(xDivLabel, null);
			pageLayoutPanel.add(divLabel2, null);
			pageLayoutPanel.add(getXGridTextField(), null);
			pageLayoutPanel.add(getYGridTextField(), null);
			pageLayoutPanel.add(divLabel1, null);
			pageLayoutPanel.add(yDivLabel, null);
	
		}
		return pageLayoutPanel;
	}

	/**
	 * This method initializes yDimTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getYDimTextField() {
		if (yDimTextField == null) {
			yDimTextField = new JTextField();
			yDimTextField.setBounds(new Rectangle(80, 70, 31, 22));
			yDimTextField.setText("11");
			yDimTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.PAGE_WIDTH = (int) Double.valueOf(xDimTextField.getText()).doubleValue()*72;
					Rise.PAGE_HEIGHT = (int) (Double.valueOf(yDimTextField.getText()).doubleValue()*72);
					Rise.PAGE_MARGIN = (int) (Double.valueOf(marginTextField.getText()).doubleValue()*72)+18;
					Rise.IMAGE_WIDTH = Rise.PAGE_WIDTH-Rise.PAGE_MARGIN*2;
					Rise.IMAGE_HEIGHT = Rise.PAGE_HEIGHT-Rise.PAGE_MARGIN*2;
					score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
					score.repaint();
					score.revalidate();
				}
			});
		}
		return yDimTextField;
	}

	/**
	 * This method initializes xDimTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getXDimTextField() {
		if (xDimTextField == null) {
			xDimTextField = new JTextField();
			xDimTextField.setBounds(new Rectangle(20, 70, 31, 22));
			xDimTextField.setText("17");
			xDimTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.PAGE_WIDTH = (int) Double.valueOf(xDimTextField.getText()).doubleValue()*72;
					Rise.PAGE_HEIGHT = (int) (Double.valueOf(yDimTextField.getText()).doubleValue()*72);
					Rise.PAGE_MARGIN = (int) (Double.valueOf(marginTextField.getText()).doubleValue()*72)+18;
					Rise.IMAGE_WIDTH = Rise.PAGE_WIDTH-Rise.PAGE_MARGIN*2;
					Rise.IMAGE_HEIGHT = Rise.PAGE_HEIGHT-Rise.PAGE_MARGIN*2;
					score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
					score.repaint();
					score.revalidate();
				}
			});
		}
		return xDimTextField;
	}

	/**
	 * This method initializes marginTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getMarginTextField() {
		if (marginTextField == null) {
			marginTextField = new JTextField();
			marginTextField.setBounds(new Rectangle(140, 70, 30, 22));
			marginTextField.setText(".5");
			marginTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.PAGE_WIDTH = (int) Double.valueOf(xDimTextField.getText()).doubleValue()*72;
					Rise.PAGE_HEIGHT = (int) (Double.valueOf(yDimTextField.getText()).doubleValue()*72);
					Rise.PAGE_MARGIN = (int) (Double.valueOf(marginTextField.getText()).doubleValue()*72)+18;
					Rise.IMAGE_WIDTH = Rise.PAGE_WIDTH-Rise.PAGE_MARGIN*2;
					Rise.IMAGE_HEIGHT = Rise.PAGE_HEIGHT-Rise.PAGE_MARGIN*2;
					score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
					score.repaint();
					score.revalidate();
				}
			});
		}
		return marginTextField;
	}

	/**
	 * This method initializes pieceVariablesPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPieceVariablesPanel() {
		if (pieceVariablesPanel == null) {
			transpositionScoreLabel = new JLabel();
			transpositionScoreLabel.setBounds(new Rectangle(230, 30, 65, 16));
			transpositionScoreLabel.setText("Transpose");
			sectionsLabel = new JLabel();
			sectionsLabel.setBounds(new Rectangle(120, 30, 53, 16));
			sectionsLabel.setText("Sections");
			numeratorLabel = new JLabel();
			numeratorLabel.setBounds(new Rectangle(10, 30, 91, 16));
			numeratorLabel.setText("1st Numerator");
			pieceVariablesLabel = new JLabel();
			pieceVariablesLabel.setBounds(new Rectangle(10, 10, 93, 16));
			pieceVariablesLabel.setText("Piece Variables");
			pieceVariablesPanel = new JPanel();
			pieceVariablesPanel.setLayout(null);
			pieceVariablesPanel.add(getNumeratorTextField(), null);
			pieceVariablesPanel.add(getSectionsTextField(), null);
			pieceVariablesPanel.add(pieceVariablesLabel, null);
			pieceVariablesPanel.add(numeratorLabel, null);
			pieceVariablesPanel.add(sectionsLabel, null);
			pieceVariablesPanel.add(transpositionScoreLabel, null);
			pieceVariablesPanel.add(getTranspositionScoreTextField(), null);
		}
		return pieceVariablesPanel;
	}

	/**
	 * This method initializes numeratorTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getNumeratorTextField() {
		if (numeratorTextField == null) {
			numeratorTextField = new JTextField();
			numeratorTextField.setBounds(new Rectangle(10, 50, 31, 22));
			numeratorTextField.setText("16");
			numeratorTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.FIRST_NUM = Integer.valueOf(numeratorTextField.getText()).intValue();
					Rise.VOICES = Integer.valueOf(sectionsTextField.getText()).intValue()+1;
					score.removeAll();
					score.add(Rise.BOUNDING_BOX);
					score.add(Rise.SCROLLBAR);
					score.setNotes();
					score.repaint();
					score.revalidate();
				}
			});
		}
		return numeratorTextField;
	}

	/**
	 * This method initializes sectionsTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getSectionsTextField() {
		if (sectionsTextField == null) {
			sectionsTextField = new JTextField();
			sectionsTextField.setBounds(new Rectangle(120, 50, 31, 22));
			sectionsTextField.setText("11");
			sectionsTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.FIRST_NUM = Integer.valueOf(numeratorTextField.getText()).intValue();
					Rise.VOICES = Integer.valueOf(sectionsTextField.getText()).intValue()+1;
					score.removeAll();
					score.add(Rise.BOUNDING_BOX);
					score.add(Rise.SCROLLBAR);
					score.setNotes();
					score.repaint();
					score.revalidate();
				}
			});
		}
		return sectionsTextField;
	}

	/**
	 * This method initializes transpositionScoreTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTranspositionScoreTextField() {
		if (transpositionScoreTextField == null) {
			transpositionScoreTextField = new JTextField();
			transpositionScoreTextField.setBounds(new Rectangle(230, 50, 30, 22));
			transpositionScoreTextField.setText("36.");
			transpositionScoreTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.TRANSPOSE_SCORE = Float.valueOf(transpositionScoreTextField.getText()).floatValue();
					Rise.LINE_THICKNESS = Float.valueOf(lineThicknessTextField.getText()).floatValue();
					Rise.FONT = Float.valueOf(fontSizeTextField.getText()).floatValue();
					Rise.PAGE_WIDTH = (int) Double.valueOf(xDimTextField.getText()).doubleValue()*72;
					Rise.PAGE_HEIGHT = (int) (Double.valueOf(yDimTextField.getText()).doubleValue()*72);
					Rise.PAGE_MARGIN = (int) (Double.valueOf(marginTextField.getText()).doubleValue()*72)+18;
					Rise.IMAGE_WIDTH = Rise.PAGE_WIDTH-Rise.PAGE_MARGIN*2;
					Rise.IMAGE_HEIGHT = Rise.PAGE_HEIGHT-Rise.PAGE_MARGIN*2;
					score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
					Rise.X_DIV = Integer.valueOf(xGridTextField.getText()).intValue();
					Rise.Y_DIV = Integer.valueOf(yGridTextField.getText()).intValue();
					Rise.CURRENT_X_PAGE = 0;
					Rise.CURRENT_Y_PAGE = 0;
					Rise.FIRST_NUM = Integer.valueOf(numeratorTextField.getText()).intValue();
					Rise.VOICES = Integer.valueOf(sectionsTextField.getText()).intValue()+1;
					score.removeAll();
					score.add(Rise.BOUNDING_BOX);
					score.add(Rise.SCROLLBAR);
					score.setNotes();
					score.repaint();
					score.revalidate();
				}
			});
		}
		return transpositionScoreTextField;
	}

	/**
	 * This method initializes formatPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getFormatPanel() {
		if (formatPanel == null) {
			lineLabel = new JLabel();
			lineLabel.setBounds(new Rectangle(80, 30, 94, 16));
			lineLabel.setText("Line Thickness");
			fontLabel = new JLabel();
			fontLabel.setBounds(new Rectangle(10, 30, 57, 16));
			fontLabel.setText("Font Size");
			formatLabel = new JLabel();
			formatLabel.setBounds(new Rectangle(10, 10, 133, 16));
			formatLabel.setText("Text and Line Format");
			formatPanel = new JPanel();
			formatPanel.setLayout(null);
			formatPanel.add(getFontSizeTextField(), null);
			formatPanel.add(getLineThicknessTextField(), null);
			formatPanel.add(formatLabel, null);
			formatPanel.add(fontLabel, null);
			formatPanel.add(lineLabel, null);
		}
		return formatPanel;
	}

	/**
	 * This method initializes fontSizeTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getFontSizeTextField() {
		if (fontSizeTextField == null) {
			fontSizeTextField = new JTextField();
			fontSizeTextField.setBounds(new Rectangle(10, 50, 29, 22));
			fontSizeTextField.setText("12");
			fontSizeTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.FONT = Float.valueOf(fontSizeTextField.getText()).floatValue();
					score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
					score.repaint();
					score.revalidate();
				}
			});
		}
		return fontSizeTextField;
	}

	/**
	 * This method initializes lineThicknessTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getLineThicknessTextField() {
		if (lineThicknessTextField == null) {
			lineThicknessTextField = new JTextField();
			lineThicknessTextField.setBounds(new Rectangle(80, 50, 28, 22));
			lineThicknessTextField.setText(".05");
			lineThicknessTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.LINE_THICKNESS = Float.valueOf(lineThicknessTextField.getText()).floatValue();
					score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
					score.repaint();
					score.revalidate();
				}
			});
		}
		return lineThicknessTextField;
	}

	/**
	 * This method initializes xPageDownEdit	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getXPageDownEdit() {
		if (xPageDownEdit == null) {
			xPageDownEdit = new JButton();
			xPageDownEdit.setBounds(new Rectangle(35, 60, 41, 26));
			xPageDownEdit.setText("<");
			xPageDownEdit.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (Rise.CURRENT_X_PAGE > 0) {
						Rise.CURRENT_X_PAGE--;
						score.removeAll();
						score.add(Rise.BOUNDING_BOX);
						score.add(Rise.SCROLLBAR);
						score.setNotes();
						score.setNoteChoices();
						score.repaint();
						score.revalidate();
					}
				}
			});
		}
		return xPageDownEdit;
	}

	/**
	 * This method initializes xPageUpEdit	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getXPageUpEdit() {
		if (xPageUpEdit == null) {
			xPageUpEdit = new JButton();
			xPageUpEdit.setBounds(new Rectangle(75, 60, 41, 26));
			xPageUpEdit.setText(">");
			xPageUpEdit.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (Rise.CURRENT_X_PAGE < Rise.X_DIV - 1) {
						Rise.CURRENT_X_PAGE++;
						score.removeAll();
						score.add(Rise.BOUNDING_BOX);
						score.add(Rise.SCROLLBAR);
						score.setNotes();
						score.setNoteChoices();
						score.repaint();
						score.revalidate();
					}
				}
			});
		}
		return xPageUpEdit;
	}

	/**
	 * This method initializes yPageUpEdit	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getYPageUpEdit() {
		if (yPageUpEdit == null) {
			yPageUpEdit = new JButton();
			yPageUpEdit.setBounds(new Rectangle(55, 35, 41, 26));
			yPageUpEdit.setText("^");
			yPageUpEdit.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (Rise.CURRENT_Y_PAGE < Rise.Y_DIV - 1) {
						Rise.CURRENT_Y_PAGE++;
						score.removeAll();
						score.add(Rise.BOUNDING_BOX);
						score.add(Rise.SCROLLBAR);
						score.setNotes();
						score.setNoteChoices();
						score.repaint();
						score.revalidate();
					}
				}
			});
		}
		return yPageUpEdit;
	}

	/**
	 * This method initializes yPageDownEdit	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getYPageDownEdit() {
		if (yPageDownEdit == null) {
			yPageDownEdit = new JButton();
			yPageDownEdit.setBounds(new Rectangle(55, 85, 41, 26));
			yPageDownEdit.setText("v");
			yPageDownEdit.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (Rise.CURRENT_Y_PAGE > 0) {
						Rise.CURRENT_Y_PAGE--;
						score.removeAll();
						score.add(Rise.BOUNDING_BOX);
						score.add(Rise.SCROLLBAR);
						score.setNotes();
						score.setNoteChoices();
						score.repaint();
						score.revalidate();
					}
				}
			});
		}
		return yPageDownEdit;
	}
	
	/**
	 * This method initializes xPageDownPlay	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getXPageDownPlay() {
		if (xPageDownPlay == null) {
			xPageDownPlay = new JButton();
			xPageDownPlay.setBounds(new Rectangle(84, 48, 41, 26));
			xPageDownPlay.setText("<");
			xPageDownPlay.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (Rise.CURRENT_X_PAGE > 0) {
						Rise.CURRENT_X_PAGE--;
						score.removeAll();
						score.add(Rise.BOUNDING_BOX);
						score.add(Rise.SCROLLBAR);
						score.setNotes();
						score.setNoteChoices();
						score.repaint();
						score.revalidate();
					}
				}
			});
		}
		return xPageDownPlay;
	}

	/**
	 * This method initializes xPageUpPlay	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getXPageUpPlay() {
		if (xPageUpPlay == null) {
			xPageUpPlay = new JButton();
			xPageUpPlay.setBounds(new Rectangle(124, 48, 41, 26));
			xPageUpPlay.setText(">");
			xPageUpPlay.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (Rise.CURRENT_X_PAGE < Rise.X_DIV - 1) {
						Rise.CURRENT_X_PAGE++;
						score.removeAll();
						score.add(Rise.BOUNDING_BOX);
						score.add(Rise.SCROLLBAR);
						score.setNotes();
						score.setNoteChoices();
						score.repaint();
						score.revalidate();
					}
				}
			});
		}
		return xPageUpPlay;
	}

	/**
	 * This method initializes yPageUpPlay	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getYPageUpPlay() {
		if (yPageUpPlay == null) {
			yPageUpPlay = new JButton();
			yPageUpPlay.setBounds(new Rectangle(104, 23, 41, 26));
			yPageUpPlay.setText("^");
			yPageUpPlay.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (Rise.CURRENT_Y_PAGE < Rise.Y_DIV - 1) {
						Rise.CURRENT_Y_PAGE++;
						score.removeAll();
						score.add(Rise.BOUNDING_BOX);
						score.add(Rise.SCROLLBAR);
						score.setNotes();
						score.setNoteChoices();
						score.repaint();
						score.revalidate();
					}
				}
			});
		}
		return yPageUpPlay;
	}

	/**
	 * This method initializes yPageDownPlay	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getYPageDownPlay() {
		if (yPageDownPlay == null) {
			yPageDownPlay = new JButton();
			yPageDownPlay.setBounds(new Rectangle(104, 73, 41, 26));
			yPageDownPlay.setText("v");
			yPageDownPlay.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (Rise.CURRENT_Y_PAGE > 0) {
						Rise.CURRENT_Y_PAGE--;
						score.removeAll();
						score.add(Rise.BOUNDING_BOX);
						score.add(Rise.SCROLLBAR);
						score.setNotes();
						score.setNoteChoices();
						score.repaint();
						score.revalidate();
					}
				}
			});
		}
		return yPageDownPlay;
	}

	/**
	 * This method initializes redrawButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getRedrawButton() {
		if (redrawButton == null) {
			redrawButton = new JButton();
			redrawButton.setBounds(new Rectangle(270, 15, 141, 29));
			redrawButton.setText("Redraw");
			redrawButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					redraw();
				}
			});
		}
		return redrawButton;
	}
	
	public void redraw(){
//		Rise.BOUNDING_BOX.setVisible(false);
		//Rise.NOTE_FRAME.setVisible(false);
		Rise.LINE_THICKNESS = Float.valueOf(lineThicknessTextField.getText()).floatValue();
		Rise.FONT = Float.valueOf(fontSizeTextField.getText()).floatValue();
		Rise.PAGE_WIDTH = (int) Double.valueOf(xDimTextField.getText()).doubleValue()*72;
		Rise.PAGE_HEIGHT = (int) (Double.valueOf(yDimTextField.getText()).doubleValue()*72);
		Rise.PAGE_MARGIN = (int) (Double.valueOf(marginTextField.getText()).doubleValue()*72)+18;
		Rise.IMAGE_WIDTH = Rise.PAGE_WIDTH-Rise.PAGE_MARGIN*2;
		Rise.IMAGE_HEIGHT = Rise.PAGE_HEIGHT-Rise.PAGE_MARGIN*2;
		score.setPreferredSize(new Dimension((int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM)));
		Rise.X_DIV = Integer.valueOf(xGridTextField.getText()).intValue();
		Rise.Y_DIV = Integer.valueOf(yGridTextField.getText()).intValue();
		Rise.CURRENT_X_PAGE = 0;
		Rise.CURRENT_Y_PAGE = 0;
		Rise.FIRST_NUM = Integer.valueOf(numeratorTextField.getText()).intValue();
		Rise.VOICES = Integer.valueOf(sectionsTextField.getText()).intValue()+1;
		score.removeAll();
		score.add(Rise.BOUNDING_BOX);
		score.add(Rise.SCROLLBAR);
		score.setNotes();
		score.repaint();
		score.revalidate();
	}

	/**
	 * This method initializes playModePanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPlayModePanel() {
		if (playModePanel == null) {
			ampLabel = new JLabel();
			ampLabel.setText("Fader");
			ampLabel.setBounds(new Rectangle(0, 20, 65, 16));
			transpositionSoundingLabel = new JLabel();
			transpositionSoundingLabel.setText("Transpose");
			transpositionSoundingLabel.setBounds(new Rectangle(0, 240, 65, 16));
			minsecLabel = new JLabel();
			minsecLabel.setText("minutes:seconds");
			minsecLabel.setBounds(new Rectangle(0, 20, 107, 16));
			colonLabel = new JLabel();
			colonLabel.setText(":");
			colonLabel.setBounds(new Rectangle(35, 45, 10, 16));
			pieceDurLabel = new JLabel();
			pieceDurLabel.setText("Piece Duration");
			pieceDurLabel.setBounds(new Rectangle(0, 0, 91, 16));
			masterLabel = new JLabel();
			masterLabel.setText("Master");
			masterLabel.setBounds(new Rectangle(0, 0, 42, 16));
			playModePanel = new JPanel();
			playModePanel.setLayout(null);
			
			zoomPlayLabel = new JLabel();
			zoomPlayLabel.setBounds(new Rectangle(236, 29, 36, 16));
			zoomPlayLabel.setText("Zoom");
			pageNavPlayLabel = new JLabel();
			pageNavPlayLabel.setBounds(new Rectangle(74, 3, 101, 16));
			pageNavPlayLabel.setText("Page Navigation");
			playModePanel.add(getZoomInButtonPlay(), null);
			playModePanel.add(getZoomOutButtonPlay(), null);
			playModePanel.add(getXPageDownPlay(), null);
			playModePanel.add(getXPageUpPlay(), null);
			playModePanel.add(getYPageUpPlay(), null);
			playModePanel.add(getYPageDownPlay(), null);
			playModePanel.add(zoomPlayLabel, null);
			playModePanel.add(pageNavPlayLabel, null);
			
			playModePanel.add(getJTabbedPane1(), null);
			playModePanel.add(getCuePanel(), null);
		}
		return playModePanel;
	}

	/**
	 * This method initializes masterAmpSlider	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getMasterAmpSlider() {
		if (masterAmpSlider == null) {
			Hashtable labelAmpTable = new Hashtable();
			labelAmpTable.put( new Integer( 1189 ), new JLabel("5") );
			labelAmpTable.put( new Integer( 1000 ), new JLabel("0 dB") );
			labelAmpTable.put( new Integer( 841 ), new JLabel("-5") );
			labelAmpTable.put( new Integer( 707 ), new JLabel("-10") );
			labelAmpTable.put( new Integer( 500 ), new JLabel("-20") );
			labelAmpTable.put( new Integer( 250 ), new JLabel("-40") );
			labelAmpTable.put( new Integer( 0 ), new JLabel("-inf") );
			masterAmpSlider = new JSlider(JSlider.VERTICAL, 0, 1189, 1000);
			masterAmpSlider.setBounds(new Rectangle(-10, 40, 71, 201));
			masterAmpSlider.setLabelTable(labelAmpTable);
			masterAmpSlider.setPaintLabels(true);
			masterAmpSlider.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					double dB = Math.log(masterAmpSlider.getValue()/1000.)/Math.log(2)*20;
					masterAmp = Math.pow(10,(dB/20.));
					//System.out.println(dB + " " + amp);
					score.setMasterFader(masterAmp);
				}
			});
		}
		return masterAmpSlider;
	}
	
	/**
	 * This method initializes saveButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getSaveButton() {
		if (saveButton == null) {
			saveButton = new JButton();
			saveButton.setBounds(new Rectangle(300, 45, 71, 26));
			saveButton.setText("Save");
			saveButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					int fd = fc.showSaveDialog(score);
					if(fd==JFileChooser.APPROVE_OPTION)
						try {
							{
								ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(fc.getSelectedFile())));
								SaveClassIterator sCI = new SaveClassIterator((Container) jTabbedPane, out);
								out.close();
							}
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				}
			});
		}
		return saveButton;
	}

	/**
	 * This method initializes openButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getOpenButton() {
		if (openButton == null) {
			openButton = new JButton();
			openButton.setBounds(new Rectangle(300, 75, 71, 26));
			openButton.setText("Open");
			openButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					int fd = fc.showOpenDialog(score);

					if(fd==JFileChooser.APPROVE_OPTION){
						ObjectInputStream in;
						try {
							in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(fc.getSelectedFile())));
							OpenClassIterator sCI = new OpenClassIterator((Container) jTabbedPane, in);
							in.close();
							redraw();
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
			}
		});
	}
	return openButton;
}

	/**
	 * This method initializes printButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getPrintButton() {
		if (printButton == null) {
			printButton = new JButton();
			printButton.setBounds(new Rectangle(300, 105, 71, 26));
			printButton.setText("Print");
			printButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					printUtilities.init(score, Rise.CURRENT_X_PAGE, Rise.CURRENT_Y_PAGE);
				}
			});
		}
		return printButton;
	}

	/**
	 * This method initializes minField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getMinTextField() {
		if (minTextField == null) {
			minTextField = new JTextField();
			minTextField.setText("27");
			minTextField.setBounds(new Rectangle(0, 40, 31, 22));
			minTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.TOTAL_DUR = 60 * Integer.valueOf(minTextField.getText()).intValue() + Integer.valueOf(secTextField.getText()).intValue();
				}
			});
		}
		return minTextField;
	}

	/**
	 * This method initializes secTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getSecTextField() {
		if (secTextField == null) {
			secTextField = new JTextField();
			secTextField.setText("30");
			secTextField.setBounds(new Rectangle(45, 40, 31, 21));
			secTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.TOTAL_DUR = 60 * Integer.valueOf(minTextField.getText()).intValue() + Integer.valueOf(secTextField.getText()).intValue();
				}
			});
		}
		return secTextField;
	}

	/**
	 * This method initializes playButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getPlayButton() {
		if (playButton == null) {
			playButton = new JButton();
			playButton.setText("Play");
			playButton.setBounds(new Rectangle(0, 200, 61, 41));
			playButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					
					//There is thread issues with double clicking start
					if (gliss.isOn == true){
						//System.out.println("MEEMEE!!!");
						gliss.kill();
					}
					
					if (Rise.IS_ENGINE_ON == true){
						Rise.IS_ENGINE_ON = false;
						Synth.stopEngine();
					}
					
					Synth.startEngine(0);
					
					//System.out.println("check " + grainSoundDurValMinFader.getValue());
					
					Rise.IS_ENGINE_ON = true;
					
					pV = new PitchVector();
					pV.init();
					
					grainStream = new GrainStream[grainDenValMaxFader.getValue()];
					for (int i = 0; i < grainStream.length; i++){
						grainStream[i] = new GrainStream();
						grainStream[i].start();
					}
					
					initializeGrains();
					
					//score.setGrainStream(grainStream);
					
					int currentTick = Synth.getTickCount();
					
					gliss.init(grainStream, grainDenValMinFader.getValue(), currentTick, grainDenValMaxFader.getValue());
					
					synchronized (gliss){
						gliss.notify();
					}
					
					gliss.setGlissFader(glissAmp);
					gliss.setGrainFader(grainAmp);
					score.setMasterFader(masterAmp);
					score.setClickFader(clickAmp);
					
					for (int i = 0; i < grainDenValMaxFader.getValue(); i++){
						synchronized (grainStream[i]){
							grainStream[i].notify();
						}
					}

					//synchronized (cue){
					//	cue.notify();
					//}
		
				}
			});
		}
		return playButton;
	}
	
	public void initializeGrains(){
		for (int i = 0; i < grainDenValMaxFader.getValue(); i++){
			grainStream[i].init(
					grainAmpValMinFader.getValue()/1000.,
					grainAmpValMaxFader.getValue()/1000.,
					grainAmpJitMinFader.getValue()/1000.,
					grainAmpJitMaxFader.getValue()/1000.,
					grainSoundDurValMinFader.getValue()/100.,
					grainSoundDurValMaxFader.getValue()/100.,
					grainSoundDurJitMinFader.getValue()/100.,
					grainSoundDurJitMaxFader.getValue()/100.,
					grainSilenceDurValMinFader.getValue()/100.,
					grainSilenceDurValMaxFader.getValue()/100.,
					grainSilenceDurJitMinFader.getValue()/100.,
					grainSilenceDurJitMaxFader.getValue()/100.,
					pV,
					grainDenValMaxFader.getValue(),
					i
			);
			
		}
	}

	/**
	 * This method initializes pauseButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getPauseButton() {
		if (pauseButton == null) {
			pauseButton = new JButton();
			pauseButton.setText("Stop");
			pauseButton.setBounds(new Rectangle(60, 200, 61, 41));
			pauseButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					gliss.kill();
//					for (int i = 0; i < grainDenValMaxFader.getValue(); i++){
//						grainStream[i].interrupt();
//						try {
//							grainStream[i].wait(0);
//						} catch (InterruptedException e1) {
//							// TODO Auto-generated catch block
//							e1.printStackTrace();
//						}
//					}
					//gliss.interrupt();
					if (Rise.IS_ENGINE_ON == true){
						Rise.IS_ENGINE_ON = false;
						Synth.stopEngine();
					}
//					synchronized (gliss) {
//						try {
//							gliss.wait();
//						} catch (InterruptedException e1) {
//							// TODO Auto-generated catch block
//							e1.printStackTrace();
//						}
//					}
					
				}
			});
		}
		return pauseButton;
	}

	/**
	 * This method initializes transpositionSoundingTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTranspositionSoundingTextField() {
		if (transpositionSoundingTextField == null) {
			transpositionSoundingTextField = new JTextField();
			transpositionSoundingTextField.setText("0.0");
			transpositionSoundingTextField.setBounds(new Rectangle(0, 260, 35, 20));
			transpositionSoundingTextField
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							Rise.TRANSPOSE_SOUND = Float.valueOf(transpositionSoundingTextField.getText()).floatValue();
						}
					});
		}
		return transpositionSoundingTextField;
	}


	/**
	 * This method initializes jTabbedPane1	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getJTabbedPane1() {
		if (jTabbedPane1 == null) {
			jTabbedPane1 = new JTabbedPane();
			jTabbedPane1.setBounds(new Rectangle(0, 105, 489, 331));
			jTabbedPane1.addTab("Master Controls", null, getMasterControlsPanel(), null);
			jTabbedPane1.addTab("Grain Values", null, getGrainValuesPanel(), null);
			jTabbedPane1.addTab("Glissandi Values", null, getGlissValuesPanel(), null);
		}
		return jTabbedPane1;
	}

	/**
	 * This method initializes masterControlsPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getMasterControlsPanel() {
		if (masterControlsPanel == null) {
			masterControlsPanel = new JPanel();
			masterControlsPanel.setLayout(null);
			masterControlsPanel.add(getTranspositionSoundingTextField(), null);
			masterControlsPanel.add(transpositionSoundingLabel, null);
			masterControlsPanel.add(getPauseButton(), null);
			masterControlsPanel.add(getPlayButton(), null);
			masterControlsPanel.add(getSecTextField(), null);
			masterControlsPanel.add(getMasterFaderPanel(), null);
			masterControlsPanel.add(getGlissFaderPanel(), null);
			masterControlsPanel.add(getGrainsFaderPanel(), null);
			masterControlsPanel.add(getTimePanel(), null);
			masterControlsPanel.add(getStartTimePanel(), null);
			masterControlsPanel.add(getSpectrumPanel(), null);
			masterControlsPanel.add(getFadePanel(), null);
			masterControlsPanel.add(getClickFaderPanel(), null);
		}
		return masterControlsPanel;
	}

	/**
	 * This method initializes grainValuesPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainValuesPanel() {
		if (grainValuesPanel == null) {
			grainValuesPanel = new JPanel();
			grainValuesPanel.setLayout(null);
			grainValuesPanel.add(getGrainAmpPanel(), null);
			grainValuesPanel.add(getGrainSoundDurPanel(), null);
			grainValuesPanel.add(getGrainSilenceDurPanel(), null);
			grainValuesPanel.add(getGrainDenPanel(), null);
		}
		return grainValuesPanel;
	}

	/**
	 * This method initializes grainAmpPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainAmpPanel() {
		if (grainAmpPanel == null) {
			grainAmpLabel = new JLabel();
			grainAmpLabel.setBounds(new Rectangle(0, 0, 96, 16));
			grainAmpLabel.setText("Amplitude");
			grainAmpPanel = new JPanel();
			grainAmpPanel.setLayout(null);
			grainAmpPanel.setBounds(new Rectangle(0, 0, 101, 271));
			grainAmpPanel.add(getGrainAmpTabPane(), null);
			grainAmpPanel.add(grainAmpLabel, null);
		}
		return grainAmpPanel;
	}

	/**
	 * This method initializes grainAmpTabPane	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getGrainAmpTabPane() {
		if (grainAmpTabPane == null) {
			grainAmpTabPane = new JTabbedPane();
			grainAmpTabPane.setBounds(new Rectangle(1, 20, 100, 251));
			grainAmpTabPane.addTab("Val", null, getGrainAmpValPanel(), null);
			grainAmpTabPane.addTab("Jit", null, getGrainAmpJitPanel(), null);
		}
		return grainAmpTabPane;
	}

	/**
	 * This method initializes grainAmpValPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainAmpValPanel() {
		if (grainAmpValPanel == null) {
			startEndGrainampValLabel = new JLabel();
			startEndGrainampValLabel.setBounds(new Rectangle(0, 0, 59, 16));
			startEndGrainampValLabel.setText("start/end");
			grainAmpValPanel = new JPanel();
			grainAmpValPanel.setLayout(null);
			grainAmpValPanel.add(getGrainAmpValMaxFader(), null);
			grainAmpValPanel.add(getGrainAmpValMinFader(), null);
			grainAmpValPanel.add(startEndGrainampValLabel, null);
		}
		return grainAmpValPanel;
	}

	/**
	 * This method initializes grainAmpValMaxFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainAmpValMaxFader() {
		if (grainAmpValMaxFader == null) {
			grainAmpValMaxFader = new JSlider(JSlider.VERTICAL, 0, 1000, 750);
			Hashtable labelAmpTable = new Hashtable();
			labelAmpTable.put( new Integer( 1000 ), new JLabel("1") );
			labelAmpTable.put( new Integer( 750 ), new JLabel(".75") );
			labelAmpTable.put( new Integer( 500 ), new JLabel(".5") );
			labelAmpTable.put( new Integer( 250 ), new JLabel(".25") );
			labelAmpTable.put( new Integer( 0 ), new JLabel("0") );
			grainAmpValMaxFader.setLabelTable(labelAmpTable);
			grainAmpValMaxFader.setBounds(new Rectangle(20, 10, 61, 201));
			grainAmpValMaxFader.setMinorTickSpacing(50);
			grainAmpValMaxFader.setPaintLabels(true);
			grainAmpValMaxFader.setPaintTicks(true);
			grainAmpValMaxFader.setMajorTickSpacing(250);
			Rise.GRAIN_AMP_VAL_END = grainAmpValMaxFader.getValue()/1000.;
			grainAmpValMaxFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainAmpValMinFader.getValue() > grainAmpValMaxFader.getValue()){
						grainAmpValMinFader.setValue(grainAmpValMaxFader.getValue());
						Rise.GRAIN_AMP_VAL_START = grainAmpValMinFader.getValue()/1000.;
					}
					Rise.GRAIN_AMP_VAL_END = grainAmpValMaxFader.getValue()/1000.;
					//initializeGrains();
				}
			});
		}
		return grainAmpValMaxFader;
	}

	/**
	 * This method initializes grainAmpValMinFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainAmpValMinFader() {
		if (grainAmpValMinFader == null) {
			grainAmpValMinFader = new JSlider(JSlider.VERTICAL, 0, 1000, 500);
			grainAmpValMinFader.setBounds(new Rectangle(0, 10, 26, 201));
			grainAmpValMinFader.setPaintLabels(true);
			Rise.GRAIN_AMP_VAL_START = grainAmpValMinFader.getValue()/1000.;
			grainAmpValMinFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainAmpValMaxFader.getValue() < grainAmpValMinFader.getValue()){
						grainAmpValMaxFader.setValue(grainAmpValMinFader.getValue());
						Rise.GRAIN_AMP_VAL_END = grainAmpValMaxFader.getValue()/1000.;
					}
					Rise.GRAIN_AMP_VAL_START = grainAmpValMinFader.getValue()/1000.;
					//initializeGrains();
				}
			});
		}
		return grainAmpValMinFader;
	}
	
	/**
	 * This method initializes grainAmpJitPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainAmpJitPanel() {
		if (grainAmpJitPanel == null) {
			startEndGrainAmpJitLabel = new JLabel();
			startEndGrainAmpJitLabel.setBounds(new Rectangle(0, 0, 61, 16));
			startEndGrainAmpJitLabel.setText("start/end");
			grainAmpJitPanel = new JPanel();
			grainAmpJitPanel.setLayout(null);
			grainAmpJitPanel.add(getGrainAmpJitMaxFader(), null);
			grainAmpJitPanel.add(getGrainAmpJitMinFader(), null);
			grainAmpJitPanel.add(startEndGrainAmpJitLabel, null);
		}
		return grainAmpJitPanel;
	}

	/**
	 * This method initializes grainAmpJitMaxFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainAmpJitMaxFader() {
		if (grainAmpJitMaxFader == null) {
			grainAmpJitMaxFader = new JSlider(JSlider.VERTICAL, 0, 1000, 250);
			Hashtable labelAmpTable = new Hashtable();
			labelAmpTable.put( new Integer( 1000 ), new JLabel("1") );
			labelAmpTable.put( new Integer( 750 ), new JLabel(".75") );
			labelAmpTable.put( new Integer( 500 ), new JLabel(".5") );
			labelAmpTable.put( new Integer( 250 ), new JLabel(".25") );
			labelAmpTable.put( new Integer( 0 ), new JLabel("0") );
			grainAmpJitMaxFader.setLabelTable(labelAmpTable);
			grainAmpJitMaxFader.setBounds(new Rectangle(20, 10, 61, 201));
			grainAmpJitMaxFader.setMinorTickSpacing(50);
			grainAmpJitMaxFader.setPaintLabels(true);
			grainAmpJitMaxFader.setPaintTicks(true);
			grainAmpJitMaxFader.setMajorTickSpacing(250);
			Rise.GRAIN_AMP_JIT_END = grainAmpJitMaxFader.getValue()/1000.;
			grainAmpJitMaxFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainAmpJitMinFader.getValue() > grainAmpJitMaxFader.getValue()){
						grainAmpJitMinFader.setValue(grainAmpJitMaxFader.getValue());
						Rise.GRAIN_AMP_JIT_START = grainAmpJitMinFader.getValue()/1000.;
					}
					Rise.GRAIN_AMP_JIT_END = grainAmpJitMaxFader.getValue()/1000.;
					//initializeGrains();
				}
			});
		}
		return grainAmpJitMaxFader;
	}

	/**
	 * This method initializes grainAmpJitMinFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainAmpJitMinFader() {
		if (grainAmpJitMinFader == null) {
			grainAmpJitMinFader = new JSlider(JSlider.VERTICAL, 0, 1000, 250);
			grainAmpJitMinFader.setBounds(new Rectangle(0, 10, 26, 201));
			grainAmpJitMinFader.setPaintLabels(true);
			Rise.GRAIN_AMP_JIT_START = grainAmpJitMinFader.getValue()/1000.;
			grainAmpJitMinFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainAmpJitMaxFader.getValue() < grainAmpJitMinFader.getValue()){
						grainAmpJitMaxFader.setValue(grainAmpJitMinFader.getValue());
						Rise.GRAIN_AMP_JIT_END = grainAmpJitMaxFader.getValue()/1000.;
					}
					Rise.GRAIN_AMP_JIT_START = grainAmpJitMinFader.getValue()/1000.;
					//initializeGrains();
				}
			});
		}
		return grainAmpJitMinFader;
	}
	

	/**
	 * This method initializes grainDenPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainDenPanel() {
		if (grainDenPanel == null) {
			grainDenLabel = new JLabel();
			grainDenLabel.setBounds(new Rectangle(0, 0, 96, 16));
			grainDenLabel.setText("Density");
			grainDenPanel = new JPanel();
			grainDenPanel.setLayout(null);
			grainDenPanel.setBounds(new Rectangle(100, 0, 101, 271));
			grainDenPanel.add(getGrainDenTabPane(), null);
			grainDenPanel.add(grainDenLabel, null);
		}
		return grainDenPanel;
	}

	/**
	 * This method initializes grainDenTabPane	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getGrainDenTabPane() {
		if (grainDenTabPane == null) {
			grainDenTabPane = new JTabbedPane();
			grainDenTabPane.setBounds(new Rectangle(1, 20, 100, 251));
			grainDenTabPane.addTab("Val", null, getGrainDenValPanel(), null);
		}
		return grainDenTabPane;
	}

	/**
	 * This method initializes grainDenValPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainDenValPanel() {
		if (grainDenValPanel == null) {
			startEndGrainampValLabel = new JLabel();
			startEndGrainampValLabel.setBounds(new Rectangle(0, 0, 59, 16));
			startEndGrainampValLabel.setText("start/end");
			grainDenValPanel = new JPanel();
			grainDenValPanel.setLayout(null);
			grainDenValPanel.add(getGrainDenValMaxFader(), null);
			grainDenValPanel.add(getGrainDenValMinFader(), null);
			grainDenValPanel.add(startEndGrainampValLabel, null);
		}
		return grainDenValPanel;
	}

	/**
	 * This method initializes grainDenValMaxFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainDenValMaxFader() {
		if (grainDenValMaxFader == null) {
			grainDenValMaxFader = new JSlider(JSlider.VERTICAL, 0, 40, 20);
			Hashtable labelDenTable = new Hashtable();
			labelDenTable.put( new Integer( 40 ), new JLabel("40") );
			labelDenTable.put( new Integer( 30 ), new JLabel("30") );
			labelDenTable.put( new Integer( 20 ), new JLabel("20") );
			labelDenTable.put( new Integer( 10 ), new JLabel("10") );
			labelDenTable.put( new Integer( 0 ), new JLabel("0") );
			grainDenValMaxFader.setLabelTable(labelDenTable);
			grainDenValMaxFader.setBounds(new Rectangle(20, 10, 61, 201));
			grainDenValMaxFader.setMinorTickSpacing(1);
			grainDenValMaxFader.setPaintLabels(true);
			grainDenValMaxFader.setPaintTicks(true);
			grainDenValMaxFader.setMajorTickSpacing(10);
			grainDenValMaxFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainDenValMinFader.getValue() > grainDenValMaxFader.getValue()){
						grainDenValMinFader.setValue(grainDenValMaxFader.getValue());
					}
				}
			});
		}
		return grainDenValMaxFader;
	}

	/**
	 * This method initializes grainDenValMinFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainDenValMinFader() {
		if (grainDenValMinFader == null) {
			grainDenValMinFader = new JSlider(JSlider.VERTICAL, 0, 40, 5);
			grainDenValMinFader.setBounds(new Rectangle(0, 10, 26, 201));
			grainDenValMinFader.setPaintLabels(true);
			grainDenValMinFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainDenValMaxFader.getValue() < grainDenValMinFader.getValue()){
						grainDenValMaxFader.setValue(grainDenValMinFader.getValue());
					}
				}
			});
		}
		return grainDenValMinFader;
	}

	/**
	 * This method initializes grainSoundDurPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainSoundDurPanel() {
		if (grainSoundDurPanel == null) {
			grainSoundDurLabel = new JLabel();
			grainSoundDurLabel.setBounds(new Rectangle(0, 0, 96, 16));
			grainSoundDurLabel.setText("Sounding Dur.");
			grainSoundDurPanel = new JPanel();
			grainSoundDurPanel.setLayout(null);
			grainSoundDurPanel.setBounds(new Rectangle(200, 0, 101, 271));
			grainSoundDurPanel.add(getGrainSoundDurTabPane(), null);
			grainSoundDurPanel.add(grainSoundDurLabel, null);
		}
		return grainSoundDurPanel;
	}

	/**
	 * This method initializes grainSoundDurTabPane	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getGrainSoundDurTabPane() {
		if (grainSoundDurTabPane == null) {
			grainSoundDurTabPane = new JTabbedPane();
			grainSoundDurTabPane.setBounds(new Rectangle(1, 20, 100, 251));
			grainSoundDurTabPane.addTab("Val", null, getGrainSoundDurValPanel(), null);
			grainSoundDurTabPane.addTab("Jit", null, getGrainSoundDurJitPanel(), null);
		}
		return grainSoundDurTabPane;
	}

	/**
	 * This method initializes grainSoundDurValPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainSoundDurValPanel() {
		if (grainSoundDurValPanel == null) {
			startEndGrainampValLabel = new JLabel();
			startEndGrainampValLabel.setBounds(new Rectangle(0, 0, 59, 16));
			startEndGrainampValLabel.setText("start/end");
			grainSoundDurValPanel = new JPanel();
			grainSoundDurValPanel.setLayout(null);
			grainSoundDurValPanel.add(getGrainSoundDurValMaxFader(), null);
			grainSoundDurValPanel.add(getGrainSoundDurValMinFader(), null);
			grainSoundDurValPanel.add(startEndGrainampValLabel, null);
		}
		return grainSoundDurValPanel;
	}

	/**
	 * This method initializes grainSoundDurValMaxFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainSoundDurValMaxFader() {
		if (grainSoundDurValMaxFader == null) {
			grainSoundDurValMaxFader = new JSlider(JSlider.VERTICAL, 0, 2000, 300);
			Hashtable labelSoundDurTable = new Hashtable();
			labelSoundDurTable.put( new Integer( 2000 ), new JLabel("20") );
			labelSoundDurTable.put( new Integer( 1500 ), new JLabel("15") );
			labelSoundDurTable.put( new Integer( 1000 ), new JLabel("10") );
			labelSoundDurTable.put( new Integer( 500 ), new JLabel("5") );
			labelSoundDurTable.put( new Integer( 0 ), new JLabel("0") );
			grainSoundDurValMaxFader.setLabelTable(labelSoundDurTable);
			grainSoundDurValMaxFader.setBounds(new Rectangle(20, 10, 61, 201));
			grainSoundDurValMaxFader.setMinorTickSpacing(100);
			grainSoundDurValMaxFader.setPaintLabels(true);
			grainSoundDurValMaxFader.setPaintTicks(true);
			grainSoundDurValMaxFader.setMajorTickSpacing(500);
			Rise.GRAIN_SOUND_DUR_VAL_END = grainSoundDurValMaxFader.getValue()/100.;
			grainSoundDurValMaxFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainSoundDurValMinFader.getValue() < grainSoundDurValMaxFader.getValue()){
						grainSoundDurValMinFader.setValue(grainSoundDurValMaxFader.getValue());
						Rise.GRAIN_SOUND_DUR_VAL_START = grainSoundDurValMinFader.getValue()/100.;
					}
					if (grainSoundDurJitMinFader.getValue() > grainSoundDurValMinFader.getValue()){
						grainSoundDurJitMinFader.setValue(grainSoundDurValMinFader.getValue());
						Rise.GRAIN_SOUND_DUR_JIT_START = grainSoundDurJitMinFader.getValue()/100.;
					}
					Rise.GRAIN_SOUND_DUR_VAL_END = grainSoundDurValMaxFader.getValue()/100.;
					//initializeGrains();
				}
			});
		}
		return grainSoundDurValMaxFader;
	}

	/**
	 * This method initializes grainSoundDurValMinFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainSoundDurValMinFader() {
		if (grainSoundDurValMinFader == null) {
			grainSoundDurValMinFader = new JSlider(JSlider.VERTICAL, 0, 2000, 1000);
			grainSoundDurValMinFader.setBounds(new Rectangle(0, 10, 26, 201));
			grainSoundDurValMinFader.setPaintLabels(true);
			Rise.GRAIN_SOUND_DUR_VAL_START = grainSoundDurValMinFader.getValue()/100.;
			grainSoundDurValMinFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainSoundDurValMaxFader.getValue() > grainSoundDurValMinFader.getValue()){
						grainSoundDurValMaxFader.setValue(grainSoundDurValMinFader.getValue());
						Rise.GRAIN_SOUND_DUR_VAL_END = grainSoundDurValMaxFader.getValue()/100.;
					}
					if (grainSoundDurJitMaxFader.getValue() > grainSoundDurValMaxFader.getValue()){
						grainSoundDurJitMaxFader.setValue(grainSoundDurValMaxFader.getValue());
						Rise.GRAIN_SOUND_DUR_JIT_END = grainSoundDurJitMaxFader.getValue()/100.;
					}
					Rise.GRAIN_SOUND_DUR_VAL_START = grainSoundDurValMinFader.getValue()/100.;
					//initializeGrains();
				}
			});
		}
		return grainSoundDurValMinFader;
	}
	
	/**
	 * This method initializes grainSoundDurJitPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainSoundDurJitPanel() {
		if (grainSoundDurJitPanel == null) {
			startEndGrainSoundDurJitLabel = new JLabel();
			startEndGrainSoundDurJitLabel.setBounds(new Rectangle(0, 0, 61, 16));
			startEndGrainSoundDurJitLabel.setText("start/end");
			grainSoundDurJitPanel = new JPanel();
			grainSoundDurJitPanel.setLayout(null);
			grainSoundDurJitPanel.add(getGrainSoundDurJitMaxFader(), null);
			grainSoundDurJitPanel.add(getGrainSoundDurJitMinFader(), null);
			grainSoundDurJitPanel.add(startEndGrainSoundDurJitLabel, null);
		}
		return grainSoundDurJitPanel;
	}

	/**
	 * This method initializes grainSoundDurJitMaxFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainSoundDurJitMaxFader() {
		if (grainSoundDurJitMaxFader == null) {
			grainSoundDurJitMaxFader = new JSlider(JSlider.VERTICAL, 0, 2000, 150);
			Hashtable labelSoundDurTable = new Hashtable();
			labelSoundDurTable.put( new Integer( 2000 ), new JLabel("20") );
			labelSoundDurTable.put( new Integer( 1500 ), new JLabel("15") );
			labelSoundDurTable.put( new Integer( 1000 ), new JLabel("10") );
			labelSoundDurTable.put( new Integer( 500 ), new JLabel("5") );
			labelSoundDurTable.put( new Integer( 0 ), new JLabel("0") );
			grainSoundDurJitMaxFader.setLabelTable(labelSoundDurTable);
			grainSoundDurJitMaxFader.setBounds(new Rectangle(20, 10, 61, 201));
			grainSoundDurJitMaxFader.setMinorTickSpacing(100);
			grainSoundDurJitMaxFader.setPaintLabels(true);
			grainSoundDurJitMaxFader.setPaintTicks(true);
			grainSoundDurJitMaxFader.setMajorTickSpacing(500);
			Rise.GRAIN_SOUND_DUR_JIT_END = grainSoundDurJitMaxFader.getValue()/100.;
			grainSoundDurJitMaxFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainSoundDurJitMinFader.getValue() < grainSoundDurJitMaxFader.getValue()){
						grainSoundDurJitMinFader.setValue(grainSoundDurJitMaxFader.getValue());
						Rise.GRAIN_SOUND_DUR_JIT_START = grainSoundDurJitMinFader.getValue()/100.;
					}
					if (grainSoundDurJitMinFader.getValue() > grainSoundDurValMinFader.getValue()){
						grainSoundDurJitMinFader.setValue(grainSoundDurValMinFader.getValue());
						Rise.GRAIN_SOUND_DUR_JIT_START = grainSoundDurJitMinFader.getValue()/100.;
					}
					Rise.GRAIN_SOUND_DUR_JIT_END = grainSoundDurJitMaxFader.getValue()/100.;
					//initializeGrains();
				}
			});
		}
		return grainSoundDurJitMaxFader;
	}

	/**
	 * This method initializes grainSoundDurJitMinFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainSoundDurJitMinFader() {
		if (grainSoundDurJitMinFader == null) {
			grainSoundDurJitMinFader = new JSlider(JSlider.VERTICAL, 0, 2000, 250);
			grainSoundDurJitMinFader.setBounds(new Rectangle(0, 10, 26, 201));
			grainSoundDurJitMinFader.setPaintLabels(true);
			Rise.GRAIN_SOUND_DUR_JIT_START = grainSoundDurJitMinFader.getValue()/100.;
			grainSoundDurJitMinFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainSoundDurJitMaxFader.getValue() > grainSoundDurJitMinFader.getValue()){
						grainSoundDurJitMaxFader.setValue(grainSoundDurJitMinFader.getValue());
						Rise.GRAIN_SOUND_DUR_JIT_END = grainSoundDurJitMaxFader.getValue()/100.;
					}
					if (grainSoundDurJitMaxFader.getValue() > grainSoundDurValMaxFader.getValue()){
						grainSoundDurJitMaxFader.setValue(grainSoundDurValMaxFader.getValue());
						Rise.GRAIN_SOUND_DUR_JIT_END = grainSoundDurJitMaxFader.getValue()/100.;
						//grainSoundDurJitMaxFader.repaint();
					}
					Rise.GRAIN_SOUND_DUR_JIT_START = grainSoundDurJitMinFader.getValue()/100.;
					//initializeGrains();
				}
			});
		}
		return grainSoundDurJitMinFader;
	}
	
	/**
	 * This method initializes grainSilenceDurPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainSilenceDurPanel() {
		if (grainSilenceDurPanel == null) {
			grainSilenceDurLabel = new JLabel();
			grainSilenceDurLabel.setBounds(new Rectangle(0, 0, 96, 16));
			grainSilenceDurLabel.setText("Silence Dur.");
			grainSilenceDurPanel = new JPanel();
			grainSilenceDurPanel.setLayout(null);
			grainSilenceDurPanel.setBounds(new Rectangle(300, 0, 101, 271));
			grainSilenceDurPanel.add(getGrainSilenceDurTabPane(), null);
			grainSilenceDurPanel.add(grainSilenceDurLabel, null);
		}
		return grainSilenceDurPanel;
	}

	/**
	 * This method initializes grainSilenceDurTabPane	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getGrainSilenceDurTabPane() {
		if (grainSilenceDurTabPane == null) {
			grainSilenceDurTabPane = new JTabbedPane();
			grainSilenceDurTabPane.setBounds(new Rectangle(1, 20, 100, 251));
			grainSilenceDurTabPane.addTab("Val", null, getGrainSilenceDurValPanel(), null);
			grainSilenceDurTabPane.addTab("Jit", null, getGrainSilenceDurJitPanel(), null);
		}
		return grainSilenceDurTabPane;
	}

	/**
	 * This method initializes grainSilenceDurValPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainSilenceDurValPanel() {
		if (grainSilenceDurValPanel == null) {
			startEndGrainampValLabel = new JLabel();
			startEndGrainampValLabel.setBounds(new Rectangle(0, 0, 59, 16));
			startEndGrainampValLabel.setText("start/end");
			grainSilenceDurValPanel = new JPanel();
			grainSilenceDurValPanel.setLayout(null);
			grainSilenceDurValPanel.add(getGrainSilenceDurValMaxFader(), null);
			grainSilenceDurValPanel.add(getGrainSilenceDurValMinFader(), null);
			grainSilenceDurValPanel.add(startEndGrainampValLabel, null);
		}
		return grainSilenceDurValPanel;
	}

	/**
	 * This method initializes grainSilenceDurValMaxFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainSilenceDurValMaxFader() {
		if (grainSilenceDurValMaxFader == null) {
			grainSilenceDurValMaxFader = new JSlider(JSlider.VERTICAL, 0, 2000, 50);
			Hashtable labelSilenceDurTable = new Hashtable();
			labelSilenceDurTable.put( new Integer( 2000 ), new JLabel("20") );
			labelSilenceDurTable.put( new Integer( 1500 ), new JLabel("15") );
			labelSilenceDurTable.put( new Integer( 1000 ), new JLabel("10") );
			labelSilenceDurTable.put( new Integer( 500 ), new JLabel("5") );
			labelSilenceDurTable.put( new Integer( 0 ), new JLabel("0") );
			grainSilenceDurValMaxFader.setLabelTable(labelSilenceDurTable);
			grainSilenceDurValMaxFader.setBounds(new Rectangle(20, 10, 61, 201));
			grainSilenceDurValMaxFader.setMinorTickSpacing(100);
			grainSilenceDurValMaxFader.setPaintLabels(true);
			grainSilenceDurValMaxFader.setPaintTicks(true);
			grainSilenceDurValMaxFader.setMajorTickSpacing(500);
			Rise.GRAIN_SILENCE_DUR_VAL_END = grainSilenceDurValMaxFader.getValue()/100.;
			grainSilenceDurValMaxFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainSilenceDurValMinFader.getValue() < grainSilenceDurValMaxFader.getValue()){
						grainSilenceDurValMinFader.setValue(grainSilenceDurValMaxFader.getValue());
						Rise.GRAIN_SILENCE_DUR_VAL_START = grainSilenceDurValMinFader.getValue()/100.;
					}
					if (grainSilenceDurJitMinFader.getValue() > grainSilenceDurValMinFader.getValue()){
						grainSilenceDurJitMinFader.setValue(grainSilenceDurValMinFader.getValue());
						Rise.GRAIN_SILENCE_DUR_JIT_START = grainSilenceDurJitMinFader.getValue()/100.;
					}
					Rise.GRAIN_SILENCE_DUR_VAL_END = grainSilenceDurValMaxFader.getValue()/100.;
					//initializeGrains();
				}
			});
		}
		return grainSilenceDurValMaxFader;
	}

	/**
	 * This method initializes grainSilenceDurValMinFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainSilenceDurValMinFader() {
		if (grainSilenceDurValMinFader == null) {
			grainSilenceDurValMinFader = new JSlider(JSlider.VERTICAL, 0, 2000, 500);
			grainSilenceDurValMinFader.setBounds(new Rectangle(0, 10, 26, 201));
			grainSilenceDurValMinFader.setPaintLabels(true);
			Rise.GRAIN_SILENCE_DUR_VAL_START = grainSilenceDurValMinFader.getValue()/100.;
			grainSilenceDurValMinFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainSilenceDurValMaxFader.getValue() > grainSilenceDurValMinFader.getValue()){
						grainSilenceDurValMaxFader.setValue(grainSilenceDurValMinFader.getValue());
						Rise.GRAIN_SILENCE_DUR_VAL_END = grainSilenceDurValMaxFader.getValue()/100.;
					}
					if (grainSilenceDurJitMaxFader.getValue() > grainSilenceDurValMaxFader.getValue()){
						grainSilenceDurJitMaxFader.setValue(grainSilenceDurValMaxFader.getValue());
						Rise.GRAIN_SILENCE_DUR_JIT_END = grainSilenceDurJitMaxFader.getValue()/100.;
					}
					Rise.GRAIN_SILENCE_DUR_VAL_START = grainSilenceDurValMinFader.getValue()/100.;
					//initializeGrains();
				}
			});
		}
		return grainSilenceDurValMinFader;
	}
	
	/**
	 * This method initializes grainSilenceDurJitPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainSilenceDurJitPanel() {
		if (grainSilenceDurJitPanel == null) {
			startEndGrainSilenceDurJitLabel = new JLabel();
			startEndGrainSilenceDurJitLabel.setBounds(new Rectangle(0, 0, 61, 16));
			startEndGrainSilenceDurJitLabel.setText("start/end");
			grainSilenceDurJitPanel = new JPanel();
			grainSilenceDurJitPanel.setLayout(null);
			grainSilenceDurJitPanel.add(getGrainSilenceDurJitMaxFader(), null);
			grainSilenceDurJitPanel.add(getGrainSilenceDurJitMinFader(), null);
			grainSilenceDurJitPanel.add(startEndGrainSilenceDurJitLabel, null);
		}
		return grainSilenceDurJitPanel;
	}

	/**
	 * This method initializes grainSilenceDurJitMaxFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainSilenceDurJitMaxFader() {
		if (grainSilenceDurJitMaxFader == null) {
			grainSilenceDurJitMaxFader = new JSlider(JSlider.VERTICAL, 0, 2000, 40);
			Hashtable labelSilenceDurTable = new Hashtable();
			labelSilenceDurTable.put( new Integer( 2000 ), new JLabel("20") );
			labelSilenceDurTable.put( new Integer( 1500 ), new JLabel("15") );
			labelSilenceDurTable.put( new Integer( 1000 ), new JLabel("10") );
			labelSilenceDurTable.put( new Integer( 500 ), new JLabel("5") );
			labelSilenceDurTable.put( new Integer( 0 ), new JLabel("0") );
			grainSilenceDurJitMaxFader.setLabelTable(labelSilenceDurTable);
			grainSilenceDurJitMaxFader.setBounds(new Rectangle(20, 10, 61, 201));
			grainSilenceDurJitMaxFader.setMinorTickSpacing(100);
			grainSilenceDurJitMaxFader.setPaintLabels(true);
			grainSilenceDurJitMaxFader.setPaintTicks(true);
			grainSilenceDurJitMaxFader.setMajorTickSpacing(500);
			Rise.GRAIN_SILENCE_DUR_JIT_END = grainSilenceDurJitMaxFader.getValue()/100.;
			grainSilenceDurJitMaxFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainSilenceDurJitMinFader.getValue() < grainSilenceDurJitMaxFader.getValue()){
						grainSilenceDurJitMinFader.setValue(grainSilenceDurJitMaxFader.getValue());
						Rise.GRAIN_SILENCE_DUR_JIT_START = grainSilenceDurJitMinFader.getValue()/100.;
					}
					if (grainSilenceDurJitMinFader.getValue() > grainSilenceDurValMinFader.getValue()){
						grainSilenceDurJitMinFader.setValue(grainSilenceDurValMinFader.getValue());
						Rise.GRAIN_SILENCE_DUR_JIT_START = grainSilenceDurJitMinFader.getValue()/100.;
					}
					Rise.GRAIN_SILENCE_DUR_JIT_END = grainSilenceDurJitMaxFader.getValue()/100.;
					//initializeGrains();
				}
			});
		}
		return grainSilenceDurJitMaxFader;
	}

	/**
	 * This method initializes grainSilenceDurJitMinFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainSilenceDurJitMinFader() {
		if (grainSilenceDurJitMinFader == null) {
			grainSilenceDurJitMinFader = new JSlider(JSlider.VERTICAL, 0, 2000, 250);
			grainSilenceDurJitMinFader.setBounds(new Rectangle(0, 10, 26, 201));
			grainSilenceDurJitMinFader.setPaintLabels(true);
			Rise.GRAIN_SILENCE_DUR_JIT_START = grainSoundDurJitMinFader.getValue()/100.;
			grainSilenceDurJitMinFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (grainSilenceDurJitMaxFader.getValue() > grainSilenceDurJitMinFader.getValue()){
						grainSilenceDurJitMaxFader.setValue(grainSilenceDurJitMinFader.getValue());
						Rise.GRAIN_SILENCE_DUR_JIT_END = grainSilenceDurJitMaxFader.getValue()/100.;
					}
					if (grainSilenceDurJitMaxFader.getValue() > grainSilenceDurValMaxFader.getValue()){
						grainSilenceDurJitMaxFader.setValue(grainSilenceDurValMaxFader.getValue());
						Rise.GRAIN_SILENCE_DUR_JIT_END = grainSilenceDurJitMaxFader.getValue()/100.;
						//grainSilenceDurJitMaxFader.repaint();
					}
					Rise.GRAIN_SILENCE_DUR_JIT_START = grainSoundDurJitMinFader.getValue()/100.;
					//initializeGrains();
				}
			});
		}
		return grainSilenceDurJitMinFader;
	}
	
  	/**
  	 * This method initializes glissAmpPanel	
  	 * 	
  	 * @return javax.swing.JPanel	
  	 */
  	private JPanel getGlissAmpPanel() {
  		if (glissAmpPanel == null) {
  			glissAmpLabel = new JLabel();
  			glissAmpLabel.setBounds(new Rectangle(0, 0, 96, 16));
  			glissAmpLabel.setText("Amplitude");
  			glissAmpPanel = new JPanel();
  			glissAmpPanel.setLayout(null);
  			glissAmpPanel.setBounds(new Rectangle(0, 0, 101, 271));
  			glissAmpPanel.add(glissAmpLabel, null);
  			glissAmpPanel.add(getGlissAmpValPanel(), null);
  		}
  		return glissAmpPanel;
  	}
  
  	/**
  	 * This method initializes glissAmpValPanel	
  	 * 	
  	 * @return javax.swing.JPanel	
  	 */
  	private JPanel getGlissAmpValPanel() {
  		if (glissAmpValPanel == null) {
  			startEndGlissampValLabel = new JLabel();
  			startEndGlissampValLabel.setBounds(new Rectangle(0, 0, 59, 16));
  			startEndGlissampValLabel.setText("start/end");
  			glissAmpValPanel = new JPanel();
  			glissAmpValPanel.setLayout(null);
  			glissAmpValPanel.setBounds(new Rectangle(0, 30, 81, 211));
  			glissAmpValPanel.add(getGlissAmpValMaxFader(), null);
  			glissAmpValPanel.add(getGlissAmpValMinFader(), null);
  			glissAmpValPanel.add(startEndGlissampValLabel, null);
  		}
  		return glissAmpValPanel;
  	}
  
  	/**
  	 * This method initializes glissAmpValMaxFader	
  	 * 	
  	 * @return javax.swing.JSlider	
  	 */
  	private JSlider getGlissAmpValMaxFader() {
  		if (glissAmpValMaxFader == null) {
  			glissAmpValMaxFader = new JSlider(JSlider.VERTICAL, 0, 1000, 750);
  			Hashtable labelAmpTable = new Hashtable();
  			labelAmpTable.put( new Integer( 1000 ), new JLabel("1") );
  			labelAmpTable.put( new Integer( 750 ), new JLabel(".75") );
  			labelAmpTable.put( new Integer( 500 ), new JLabel(".5") );
  			labelAmpTable.put( new Integer( 250 ), new JLabel(".25") );
  			labelAmpTable.put( new Integer( 0 ), new JLabel("0") );
  			glissAmpValMaxFader.setLabelTable(labelAmpTable);
  			glissAmpValMaxFader.setBounds(new Rectangle(20, 10, 61, 201));
  			glissAmpValMaxFader.setMinorTickSpacing(50);
  			glissAmpValMaxFader.setPaintLabels(true);
  			glissAmpValMaxFader.setPaintTicks(true);
  			glissAmpValMaxFader.setMajorTickSpacing(250);
  			Rise.GLISS_AMP_END = glissAmpValMaxFader.getValue()/1000.;
  			glissAmpValMaxFader.addChangeListener(new javax.swing.event.ChangeListener() {
  				public void stateChanged(javax.swing.event.ChangeEvent e) {
  					if (glissAmpValMinFader.getValue() > glissAmpValMaxFader.getValue()){
  						glissAmpValMinFader.setValue(glissAmpValMaxFader.getValue());
  					}
  					Rise.GLISS_AMP_START = glissAmpValMinFader.getValue()/1000.;
  					Rise.GLISS_AMP_END = glissAmpValMaxFader.getValue()/1000.;
  				}
  			});
  		}
  		return glissAmpValMaxFader;
  	}
  
  	/**
  	 * This method initializes glissAmpValMinFader	
  	 * 	
  	 * @return javax.swing.JSlider	
  	 */
  	private JSlider getGlissAmpValMinFader() {
  		if (glissAmpValMinFader == null) {
  			glissAmpValMinFader = new JSlider(JSlider.VERTICAL, 0, 1000, 100);
  			glissAmpValMinFader.setBounds(new Rectangle(0, 10, 26, 201));
  			glissAmpValMinFader.setPaintLabels(true);
  			Rise.GLISS_AMP_START = glissAmpValMinFader.getValue()/1000.;
  			glissAmpValMinFader.addChangeListener(new javax.swing.event.ChangeListener() {
  				public void stateChanged(javax.swing.event.ChangeEvent e) {
  					if (glissAmpValMaxFader.getValue() < glissAmpValMinFader.getValue()){
  						glissAmpValMaxFader.setValue(glissAmpValMinFader.getValue());
  					}
  					Rise.GLISS_AMP_START = glissAmpValMinFader.getValue()/1000.;
  					Rise.GLISS_AMP_END = glissAmpValMaxFader.getValue()/1000.;
  				}
  			});
  		}
  		return glissAmpValMinFader;
  	}
  	
  
  	/**
	 * This method initializes glissValuesPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGlissValuesPanel() {
		if (glissValuesPanel == null) {
			glissValuesPanel = new JPanel();
			glissValuesPanel.setLayout(null);
			glissValuesPanel.add(getGlissAmpPanel(), null);
		}
		return glissValuesPanel;
	}

	/**
	 * This method initializes spectrumPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getSpectrumPanel() {
		if (spectrumPanel == null) {
			spectrumLabel = new JLabel();
			spectrumLabel.setBounds(new Rectangle(0, 0, 96, 16));
			spectrumLabel.setText("Spectrum");
			spectrumPanel = new JPanel();
			spectrumPanel.setLayout(null);
			spectrumPanel.setBounds(new Rectangle(365, 8, 101, 271));
			spectrumPanel.add(getSpectrumTabPane(), null);
			spectrumPanel.add(spectrumLabel, null);
		}
		return spectrumPanel;
	}

	/**
	 * This method initializes spectrumTabPane	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getSpectrumTabPane() {
		if (spectrumTabPane == null) {
			spectrumTabPane = new JTabbedPane();
			spectrumTabPane.setBounds(new Rectangle(1, 20, 100, 251));
			spectrumTabPane.addTab("Val", null, getSpectrumValPanel(), null);
		}
		return spectrumTabPane;
	}

	/**
	 * This method initializes spectrumValPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getSpectrumValPanel() {
		if (spectrumValPanel == null) {
			minMaxSpectrumValLabel = new JLabel();
			minMaxSpectrumValLabel.setBounds(new Rectangle(0, 0, 76, 16));
			minMaxSpectrumValLabel.setText("min/max");
			spectrumValPanel = new JPanel();
			spectrumValPanel.setLayout(null);
			spectrumValPanel.add(getSpectrumMaxValFader(), null);
			spectrumValPanel.add(getSpectrumMinValFader(), null);
			spectrumValPanel.add(minMaxSpectrumValLabel, null);
		}
		return spectrumValPanel;
	}

	/**
	 * This method initializes spectrumMaxValFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getSpectrumMaxValFader() {
		if (spectrumMaxValFader == null) {
			spectrumMaxValFader = new JSlider(JSlider.VERTICAL, 0, 1000, 250);
			Hashtable labelSpectrumTable = new Hashtable();
			labelSpectrumTable.put( new Integer( 1000 ), new JLabel("saw") );
			labelSpectrumTable.put( new Integer( 0 ), new JLabel("sine") );
			spectrumMaxValFader.setLabelTable(labelSpectrumTable);
			spectrumMaxValFader.setBounds(new Rectangle(20, 10, 61, 201));
			spectrumMaxValFader.setMinorTickSpacing(50);
			spectrumMaxValFader.setPaintLabels(true);
			spectrumMaxValFader.setPaintTicks(true);
			spectrumMaxValFader.setMajorTickSpacing(250);
			spectrumMaxValFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (spectrumMinValFader.getValue() > spectrumMaxValFader.getValue()){
						spectrumMinValFader.setValue(spectrumMaxValFader.getValue());
					}
					Rise.MAX_SPECTRUM = spectrumMaxValFader.getValue()/1000.;
				}
			});
		}
		return spectrumMaxValFader;
	}

	/**
	 * This method initializes spectrumMinValFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getSpectrumMinValFader() {
		if (spectrumMinValFader == null) {
			spectrumMinValFader = new JSlider(JSlider.VERTICAL, 0, 1000, 0);
			spectrumMinValFader.setBounds(new Rectangle(0, 10, 26, 201));
			spectrumMinValFader.setPaintLabels(true);
			spectrumMinValFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (spectrumMaxValFader.getValue() < spectrumMinValFader.getValue()){
						spectrumMaxValFader.setValue(spectrumMinValFader.getValue());
						Rise.MIN_SPECTRUM = spectrumMinValFader.getValue()/1000.;
					}
					Rise.MIN_SPECTRUM = spectrumMinValFader.getValue()/1000.;
				}
			});
		}
		return spectrumMinValFader;
	}

	/**
	 * This method initializes toolsTabbedPane	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getToolsTabbedPane() {
		if (toolsTabbedPane == null) {
			toolsTabbedPane = new JTabbedPane();
			toolsTabbedPane.setBounds(new Rectangle(0, 150, 436, 181));
			toolsTabbedPane.addTab("Piece Variables", null, getPieceVariablesPanel(), null);
			toolsTabbedPane.addTab("Page Layout", null, getPageLayoutPanel(), null);
			toolsTabbedPane.addTab("Text Format", null, getFormatPanel(), null);
		}
		return toolsTabbedPane;
	}

	/**
	 * This method initializes masterFaderPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getMasterFaderPanel() {
		if (masterFaderPanel == null) {
			masterFaderPanel = new JPanel();
			masterFaderPanel.setLayout(null);
			masterFaderPanel.setBounds(new Rectangle(120, 10, 56, 271));
			masterFaderPanel.add(getMasterAmpSlider(), null);
			masterFaderPanel.add(masterLabel, null);
			masterFaderPanel.add(ampLabel, null);
		}
		return masterFaderPanel;
	}

	/**
	 * This method initializes glissFaderPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGlissFaderPanel() {
		if (glissFaderPanel == null) {
			glissFaderLabel2 = new JLabel();
			glissFaderLabel2.setBounds(new Rectangle(0, 20, 65, 16));
			glissFaderLabel2.setText("Fader");
			glissFaderLabel = new JLabel();
			glissFaderLabel.setBounds(new Rectangle(0, 0, 58, 16));
			glissFaderLabel.setText("Gliss.");
			glissFaderPanel = new JPanel();
			glissFaderPanel.setLayout(null);
			glissFaderPanel.setBounds(new Rectangle(180, 10, 56, 271));
			glissFaderPanel.add(getGlissFader(), null);
			glissFaderPanel.add(glissFaderLabel, null);
			glissFaderPanel.add(glissFaderLabel2, null);
		}
		return glissFaderPanel;
	}

	/**
	 * This method initializes glissFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGlissFader() {
		if (glissFader == null) {
			Hashtable labelAmpTable = new Hashtable();
			labelAmpTable.put( new Integer( 1189 ), new JLabel("5") );
			labelAmpTable.put( new Integer( 1000 ), new JLabel("0 dB") );
			labelAmpTable.put( new Integer( 841 ), new JLabel("-5") );
			labelAmpTable.put( new Integer( 707 ), new JLabel("-10") );
			labelAmpTable.put( new Integer( 500 ), new JLabel("-20") );
			labelAmpTable.put( new Integer( 250 ), new JLabel("-40") );
			labelAmpTable.put( new Integer( 0 ), new JLabel("-inf") );
			glissFader = new JSlider(JSlider.VERTICAL, 0, 1189, 0);
			glissFader.setBounds(new Rectangle(-10, 40, 71, 201));
			glissFader.setLabelTable(labelAmpTable);
			glissFader.setPaintLabels(true);
			glissFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					double dB = Math.log(glissFader.getValue()/1000.)/Math.log(2)*20;
					glissAmp = Math.pow(10,(dB/20.));
					//System.out.println(dB + " " + amp);
					gliss.setGlissFader(glissAmp);
					//gliss.setMasterfader(amp);
					//score.clickAndPlay.amplitude.set(Rise.MASTER_AMP);
				}
			});
		}
		return glissFader;
	}

	/**
	 * This method initializes grainsFaderPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGrainsFaderPanel() {
		if (grainsFaderPanel == null) {
			grainFaderLabel2 = new JLabel();
			grainFaderLabel2.setBounds(new Rectangle(0, 20, 65, 16));
			grainFaderLabel2.setText("Fader");
			grainFaderLabel = new JLabel();
			grainFaderLabel.setBounds(new Rectangle(0, 0, 74, 16));
			grainFaderLabel.setText("Grains");
			grainsFaderPanel = new JPanel();
			grainsFaderPanel.setLayout(null);
			grainsFaderPanel.setBounds(new Rectangle(240, 10, 56, 271));
			grainsFaderPanel.add(getGrainFader(), null);
			grainsFaderPanel.add(grainFaderLabel, null);
			grainsFaderPanel.add(grainFaderLabel2, null);
		}
		return grainsFaderPanel;
	}

	/**
	 * This method initializes grainFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getGrainFader() {
		if (grainFader == null) {
			Hashtable labelAmpTable = new Hashtable();
			labelAmpTable.put( new Integer( 1189 ), new JLabel("5") );
			labelAmpTable.put( new Integer( 1000 ), new JLabel("0 dB") );
			labelAmpTable.put( new Integer( 841 ), new JLabel("-5") );
			labelAmpTable.put( new Integer( 707 ), new JLabel("-10") );
			labelAmpTable.put( new Integer( 500 ), new JLabel("-20") );
			labelAmpTable.put( new Integer( 250 ), new JLabel("-40") );
			labelAmpTable.put( new Integer( 0 ), new JLabel("-inf") );
			grainFader = new JSlider(JSlider.VERTICAL, 0, 1189, 0);
			grainFader.setBounds(new Rectangle(-10, 40, 71, 201));
			grainFader.setLabelTable(labelAmpTable);
			grainFader.setPaintLabels(true);
			grainFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					double dB = Math.log(grainFader.getValue()/1000.)/Math.log(2)*20;
					grainAmp = Math.pow(10,(dB/20.));
					//System.out.println(dB + " " + amp);
					gliss.setGrainFader(grainAmp);
					//gliss.setMasterfader(amp);
				}
			});
		}
		return grainFader;
	}

	/**
	 * This method initializes timePanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getTimePanel() {
		if (timePanel == null) {
			timePanel = new JPanel();
			timePanel.setLayout(null);
			timePanel.setBounds(new Rectangle(0, 10, 111, 61));
			timePanel.add(pieceDurLabel, null);
			timePanel.add(minsecLabel, null);
			timePanel.add(getMinTextField(), null);
			timePanel.add(colonLabel, null);
			timePanel.add(getSecTextField(), null);
		}
		return timePanel;
	}

	/**
	 * This method initializes startTimePanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getStartTimePanel() {
		if (startTimePanel == null) {
			colonStartLabel = new JLabel();
			colonStartLabel.setBounds(new Rectangle(35, 45, 10, 16));
			colonStartLabel.setText(":");
			minsecStartLabel = new JLabel();
			minsecStartLabel.setBounds(new Rectangle(0, 20, 107, 16));
			minsecStartLabel.setText("minutes:seconds");
			startTimeLabel = new JLabel();
			startTimeLabel.setBounds(new Rectangle(0, 0, 91, 16));
			startTimeLabel.setText("Start From:");
			startTimePanel = new JPanel();
			startTimePanel.setLayout(null);
			startTimePanel.setBounds(new Rectangle(0, 140, 111, 61));
			startTimePanel.add(startTimeLabel, null);
			startTimePanel.add(minsecStartLabel, null);
			startTimePanel.add(getMinStartTextField(), null);
			startTimePanel.add(colonStartLabel, null);
			startTimePanel.add(getSecStartTextField(), null);
		}
		return startTimePanel;
	}

	/**
	 * This method initializes minStartTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getMinStartTextField() {
		if (minStartTextField == null) {
			minStartTextField = new JTextField();
			minStartTextField.setBounds(new Rectangle(0, 40, 31, 22));
			minStartTextField.setText("0");
			minStartTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.START_TIME = 60 * Integer.valueOf(minStartTextField.getText()).intValue() + Integer.valueOf(secStartTextField.getText()).intValue();
					Rise.SCROLLBAR.xPos = (Rise.START_TIME/Rise.TOTAL_DUR) * Rise.SCROLLBAR_WIDTH;
					Rise.SCROLLBAR.repaint();
				}
			});
		}
		return minStartTextField;
	}

	/**
	 * This method initializes secStartTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getSecStartTextField() {
		if (secStartTextField == null) {
			secStartTextField = new JTextField();
			secStartTextField.setBounds(new Rectangle(45, 40, 31, 21));
			secStartTextField.setText("00");
			secStartTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.START_TIME = 60 * Integer.valueOf(minStartTextField.getText()).intValue() + Integer.valueOf(secStartTextField.getText()).intValue();
					Rise.SCROLLBAR.xPos = (Rise.START_TIME/Rise.TOTAL_DUR) * Rise.SCROLLBAR_WIDTH;
					Rise.SCROLLBAR.repaint();
				}
			});
		}
		return secStartTextField;
	}

	/**
	 * This method initializes fadePanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getFadePanel() {
		if (fadePanel == null) {
			colonStartLabel1 = new JLabel();
			colonStartLabel1.setBounds(new Rectangle(35, 45, 10, 16));
			colonStartLabel1.setText(":");
			minsecFadeLabel = new JLabel();
			minsecFadeLabel.setBounds(new Rectangle(0, 20, 107, 16));
			minsecFadeLabel.setText("minutes:seconds");
			fadeOutLabe = new JLabel();
			fadeOutLabe.setBounds(new Rectangle(0, 0, 91, 16));
			fadeOutLabe.setText("Fade Out");
			fadePanel = new JPanel();
			fadePanel.setLayout(null);
			fadePanel.setBounds(new Rectangle(0, 75, 111, 61));
			fadePanel.add(fadeOutLabe, null);
			fadePanel.add(minsecFadeLabel, null);
			fadePanel.add(getMinFadeField(), null);
			fadePanel.add(colonStartLabel1, null);
			fadePanel.add(getSecFadeField(), null);
		}
		return fadePanel;
	}

	/**
	 * This method initializes minFadeField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getMinFadeField() {
		if (minFadeField == null) {
			minFadeField = new JTextField();
			minFadeField.setBounds(new Rectangle(0, 40, 31, 22));
			minFadeField.setText("2");
			minFadeField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.FADE_DUR = 60 * Integer.valueOf(minFadeField.getText()).intValue() + Integer.valueOf(secFadeField.getText()).intValue();
				}
			});
		}
		return minFadeField;
	}

	/**
	 * This method initializes secFadeField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getSecFadeField() {
		if (secFadeField == null) {
			secFadeField = new JTextField();
			secFadeField.setBounds(new Rectangle(45, 40, 31, 21));
			secFadeField.setText("30");
			secFadeField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Rise.FADE_DUR = 60 * Integer.valueOf(minFadeField.getText()).intValue() + Integer.valueOf(secFadeField.getText()).intValue();
				}
			});

		}
		return secFadeField;
	}

	/**
	 * This method initializes cuePanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getCuePanel() {
		if (cuePanel == null) {
			Rise.CUE_PANEL = new CuePanel();
			cuePanel = Rise.CUE_PANEL;
			cuePanel.setLayout(null);
			cuePanel.setBackground(Color.WHITE);
			cuePanel.setBounds(new Rectangle(315, 15, 121, 76));
		}
		return cuePanel;
	}

	/**
	 * This method initializes clickFaderPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getClickFaderPanel() {
		if (clickFaderPanel == null) {
			grainFaderLabel21 = new JLabel();
			grainFaderLabel21.setBounds(new Rectangle(0, 20, 65, 16));
			grainFaderLabel21.setText("Fader");
			clickFaderLabel = new JLabel();
			clickFaderLabel.setBounds(new Rectangle(0, 0, 74, 16));
			clickFaderLabel.setText("Click");
			clickFaderPanel = new JPanel();
			clickFaderPanel.setLayout(null);
			clickFaderPanel.setBounds(new Rectangle(300, 10, 61, 269));
			clickFaderPanel.add(getClickFader(), null);
			clickFaderPanel.add(clickFaderLabel, null);
			clickFaderPanel.add(grainFaderLabel21, null);
		}
		return clickFaderPanel;
	}

	/**
	 * This method initializes clickFader	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getClickFader() {
		if (clickFader == null) {
			Hashtable labelAmpTable = new Hashtable();
			labelAmpTable.put( new Integer( 1189 ), new JLabel("5") );
			labelAmpTable.put( new Integer( 1000 ), new JLabel("0 dB") );
			labelAmpTable.put( new Integer( 841 ), new JLabel("-5") );
			labelAmpTable.put( new Integer( 707 ), new JLabel("-10") );
			labelAmpTable.put( new Integer( 500 ), new JLabel("-20") );
			labelAmpTable.put( new Integer( 250 ), new JLabel("-40") );
			labelAmpTable.put( new Integer( 0 ), new JLabel("-inf") );
			
			clickFader = new JSlider(JSlider.VERTICAL, 0, 1189, 1000);
			clickFader.setBounds(new Rectangle(-10, 40, 71, 201));
			clickFader.setLabelTable(labelAmpTable);
			clickFader.setPaintLabels(true);
			clickFader.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					double dB = Math.log(clickFader.getValue()/1000.)/Math.log(2)*20;
					clickAmp = Math.pow(10,(dB/20.));
					//System.out.println(dB + " " + amp);
					score.setClickFader(clickAmp);
					//gliss.setMasterfader(amp);
				}
			});
		}
		return clickFader;
	}

}  //  @jve:decl-index=0:visual-constraint="15,12"
