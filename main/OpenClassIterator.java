package main;

import java.awt.Container;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.JSlider;
import javax.swing.JTextField;

public class OpenClassIterator {
	
public OpenClassIterator(Container c, ObjectInputStream in){
		
		if (c.getClass() == new JTextField().getClass()){
			try {
				((JTextField) c).setText((String) in.readObject());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		else if (c.getClass() == new JSlider().getClass()){
			try {
				((JSlider) c).setValue(in.readInt());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for (int i = 0; i < c.getComponentCount(); i++){
			OpenClassIterator oCI = new OpenClassIterator((Container) c.getComponent(i),in);
		}
	}

}
