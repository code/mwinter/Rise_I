package main;

import java.awt.Container;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

public class SaveClassIterator {

	public SaveClassIterator(Container c, ObjectOutputStream out){
		
		if (c.getClass() == new JTextField().getClass()){
			try {
				out.writeObject(((JTextField) c).getText());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		else if (c.getClass() == new JSlider().getClass()){
			try {
				out.writeInt(((JSlider) c).getValue());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for (int i = 0; i < c.getComponentCount(); i++){
			SaveClassIterator sCI = new SaveClassIterator((Container) c.getComponent(i),out);
		}
	}
}
