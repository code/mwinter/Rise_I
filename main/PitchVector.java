package main;

import java.util.Vector;

import com.softsynth.jsyn.EqualTemperedTuning;

public class PitchVector{
	
	Vector[] array;
	
	public PitchVector(){
		
	}
	
	public void init(){
		
		int aLength = (int) (Rise.TOTAL_DUR * 10);
		array = new Vector[aLength];
		
		double fund = EqualTemperedTuning.getMIDIFrequency(24+Rise.TRANSPOSE_SCORE+Rise.TRANSPOSE_SOUND);
		
		for (int i = 0; i < aLength; i++){
			array[i] = new Vector();
			array[i].add(fund);
		}
		for (int plotPoint = 1; plotPoint < Rise.VOICES; plotPoint++){
			
			//int plotPoint = 
			for (int voice = 1; voice <= plotPoint; voice++){
				double x = (1./(Rise.VOICES-1))*plotPoint;
				double pitch = 1200./Math.log(2.)*Math.log((double) (Rise.FIRST_NUM+voice-plotPoint)/(Rise.FIRST_NUM-plotPoint));
				double frequency = EqualTemperedTuning.getMIDIFrequency(24+Rise.TRANSPOSE_SCORE+Rise.TRANSPOSE_SOUND+pitch/100.);
				
				int start = (int) ((x - ((1./(Rise.VOICES-1))*(plotPoint-voice)/plotPoint)) * (aLength-1));
				int end = (int) ((x + ((1./(Rise.VOICES-1))*voice/(plotPoint+1))) * (aLength-1));
				
				if (end >= aLength){
					end = aLength;
				}
				
				//System.out.println("s and e " + start + " " + end + " " + pitch);
				
				for (int i = start; i < end; i++) {
					array[i].add(frequency);
				}
				
				//System.out.println("notes 2 " + plotPoint + " " + pitch + " " + frequency +  (x - ((1./(Rise.VOICES-1))*(plotPoint-voice)/plotPoint)) + " " + (x + ((1./(Rise.VOICES-1))*voice/(plotPoint+1))));
				// p is plot point, v is voice
			}
		}	
		
	}

}
