package main;

import java.awt.Color;

import javax.swing.JPanel;

import com.softsynth.jsyn.Synth;

public class Cue extends Thread {

	JPanel cuePanel;
	int startTick;
	int cuesPast;
	int clicksPast;
	int[] cuesPoints;


	public Cue(JPanel cP){
		super();
		cuePanel = cP;
		//add(new LineOut());
	}
	
	public void init(int[] cP){
		//startTick = sT;
		cuesPoints = cP;
		cuesPast = 0;
		clicksPast = 0;
	}

	public synchronized void run() {

		try {
			//System.out.println("waiting");
			this.wait(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		//boolean isOn = true;

		int startTime = cuesPoints[0];

		int duration = (int) (Synth.getTickRate() / 10);
		//int duration = (int) (Synth.getTickRate() / 50);

		int advanceTime = (int) (Synth.getTickRate() * .5); // half second advance
		Synth.sleepUntilTick(startTime - advanceTime);// /*(int) (Math.random() * 2 * Synth.getTickRate())*/);  // Wake up early!
		int nextTime = startTime;	
		int nextCueTime = startTime;	
		while(true) 
		{
			if (Rise.IS_ENGINE_ON == true) {
				
				if (Synth.getTickCount() > nextCueTime) {
					if (cuesPast % 2 == 0) {
						cuePanel.setBackground(Color.BLACK);
					}
					else{
						cuePanel.setBackground(Color.WHITE);
					}
					cuesPast++;
					nextCueTime = cuesPoints[cuesPast];
					//System.out.println("test " + clicksPast + " " + nextCueTime + " " + Synth.getTickCount());
				}
				
				nextTime += duration;    
				Synth.sleepUntilTick(nextTime);
				
			}
			else {
				try {
					//System.out.println("waiting");
					this.wait(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
