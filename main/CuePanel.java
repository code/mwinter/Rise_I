package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;

import javax.swing.JPanel;

public class CuePanel extends JPanel{
	
	public double envPos;
	public Color color;
//	public void setEnv(double eV){
//		envPos
//	}
	
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		FontRenderContext frc = g2.getFontRenderContext();
		Font fontTime = new Font("Times", Font.PLAIN, 40);
		int totalSecs = (int) (envPos * Rise.TOTAL_DUR);
		int minutes = totalSecs/60;
		int seconds = totalSecs%60;
		String secondString = seconds + "";
		if (seconds < 10){
			secondString = "0" + secondString;
		}
		String timeForString = minutes + ":" + secondString;
		TextLayout time = new TextLayout(timeForString, fontTime, frc);
		g2.setFont(fontTime);
		
		g2.setColor(color);
		time.draw(g2,5,50);
	}

}
