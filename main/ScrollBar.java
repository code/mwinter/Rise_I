package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import javax.swing.JPanel;

public class ScrollBar extends JPanel{
	
	double xPos;
	
	public ScrollBar() {
		this.setOpaque(false);
		this.setBounds((int) 0, (int) 0,(int) (Rise.PAGE_WIDTH*Rise.ZOOM), (int) (Rise.PAGE_HEIGHT*Rise.ZOOM));
		this.setVisible(false);
	}
	
	public void paint(Graphics g){
		g.setColor(Color.BLUE);
		Graphics2D g2 = (Graphics2D) g;
		g2.draw(new Line2D.Double(Rise.PAGE_MARGIN*Rise.ZOOM + xPos,0, Rise.PAGE_MARGIN*Rise.ZOOM + xPos,Rise.PAGE_HEIGHT*Rise.ZOOM));
	}

}
