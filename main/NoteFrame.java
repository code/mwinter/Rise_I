package main;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class NoteFrame extends JFrame implements ActionListener{
	
	JRadioButton bass = new JRadioButton("bass");
	JRadioButton tenor = new JRadioButton("tenor");
	JRadioButton alto = new JRadioButton("alto");
	JRadioButton treble = new JRadioButton("treble");
	JRadioButton clefNada = new JRadioButton("nada");
	
	JRadioButton ma = new JRadioButton("15ma");
	JRadioButton va = new JRadioButton("8va");
	JRadioButton none = new JRadioButton("none");
	JRadioButton vb = new JRadioButton("8vb");
	JRadioButton mb = new JRadioButton("15mb");
	JRadioButton oBNada = new JRadioButton("nada");
	
	JRadioButton sharps = new JRadioButton("sharps");
	JRadioButton flats = new JRadioButton("flats");
	JRadioButton spellNada = new JRadioButton("nada");
	
	JRadioButton show = new JRadioButton("show");
	JRadioButton hide = new JRadioButton("hide");
	JRadioButton ratiosNada = new JRadioButton("nada");
	
	ArrayList notes;
	ArrayList listeningNotes;
	
	int clefChoice;
	int octavaAndBassoChoice;
	int ratioChoice;
	int spellingChoice;
	
	public NoteFrame(){
		
		this.setTitle("Note Frame");
		
		this.addWindowListener(new WindowAdapter()
				{
				      public void windowClosing(WindowEvent we){
				    	  Rise.BOUNDING_BOX.setVisible(false);
				    	  for (int i = 0; i < notes.size(); i++) {
				    		  ((Note) notes.get(i)).color = Color.BLACK;
				    	  }
				       }
				});
		
		Container noteContainer = this.getContentPane();
		noteContainer.setLayout(new GridLayout(0,2));
		
		bass.setActionCommand("bass");
		bass.addActionListener(this);
		tenor.setActionCommand("tenor");
		tenor.addActionListener(this);
		alto.setActionCommand("alto");
		alto.addActionListener(this);
		treble.setActionCommand("treble");
		treble.addActionListener(this);
		
		ma.setActionCommand("ma");
		ma.addActionListener(this);
		va.setActionCommand("va");
		va.addActionListener(this);
		none.setActionCommand("none");
		none.addActionListener(this);
		vb.setActionCommand("vb");
		vb.addActionListener(this);
		mb.setActionCommand("mb");
		mb.addActionListener(this);
		
		sharps.setActionCommand("sharps");
		sharps.addActionListener(this);
		flats.setActionCommand("flats");
		flats.addActionListener(this);
		
		show.setActionCommand("show");
		show.addActionListener(this);
		hide.setActionCommand("hide");
		hide.addActionListener(this);
		
		ButtonGroup clef = new ButtonGroup();
		clef.add(bass);
		clef.add(tenor);
		clef.add(alto);
		clef.add(treble);
		clef.add(clefNada);
		//clefNada.setVisible(false);
		
		ButtonGroup oB = new ButtonGroup();
		oB.add(ma);
		oB.add(va);
		oB.add(none);
		oB.add(vb);
		oB.add(mb);
		oB.add(oBNada);
		//oBNada.setVisible(false);
		
		ButtonGroup spell = new ButtonGroup();
		spell.add(sharps);
		spell.add(flats);
		spell.add(spellNada);
		//spellNada.setVisible(false);
		
		ButtonGroup ratios = new ButtonGroup();
		ratios.add(show);
		ratios.add(hide);
		ratios.add(ratiosNada);
		//ratiosNada.setVisible(false);
		
		JPanel clefPanel = new JPanel();
		clefPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		clefPanel.setLayout(new GridLayout(0,1));
		clefPanel.add(new JLabel("Clef"));
		clefPanel.add(bass);
		clefPanel.add(tenor);
		clefPanel.add(alto);
		clefPanel.add(treble);
		
		JPanel oBPanel = new JPanel();
		oBPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		oBPanel.setLayout(new GridLayout(0,1));
		oBPanel.add(new JLabel("Octava/Basso"));
		oBPanel.add(ma);
		oBPanel.add(va);
		oBPanel.add(none);
		oBPanel.add(vb);
		oBPanel.add(mb);
		
		JPanel spellPanel = new JPanel();
		spellPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		spellPanel.setLayout(new GridLayout(0,1));
		spellPanel.add(new JLabel("Spelling Pref."));
		spellPanel.add(sharps);
		spellPanel.add(flats);
		
		JPanel ratiosPanel = new JPanel();
		ratiosPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		ratiosPanel.setLayout(new GridLayout(0,1));
		ratiosPanel.add(new JLabel("Ratios"));
		ratiosPanel.add(show);
		ratiosPanel.add(hide);
		
		noteContainer.add(clefPanel);
		noteContainer.add(oBPanel);
		noteContainer.add(spellPanel);
		noteContainer.add(ratiosPanel);
		
		this.setSize(200,400);
		this.setLocation(0, 0);
	}
	
	public void setNoteAccess(ArrayList n){
		notes = n;
	}
	
	public void setListeningNotes(){
		listeningNotes = new ArrayList();
		for (int i = 0; i < notes.size(); i++) {
			if (((Note) notes.get(i)).isListening == true){
				//System.out.println(((Note) notes.get(i)).pitch);
				listeningNotes.add(notes.get(i));
			}
		}
	}

	public void actionPerformed(ActionEvent e) {
		if ("bass".equals(e.getActionCommand())){
			clefChoice = 0;
		}
		else if ("tenor".equals(e.getActionCommand())){
			clefChoice = 1;
		}
		else if ("alto".equals(e.getActionCommand())){
			clefChoice = 2;
		}
		else if ("treble".equals(e.getActionCommand())){
			clefChoice = 3;
		}
		
		else if ("mb".equals(e.getActionCommand())){
			octavaAndBassoChoice = 0;
		}
		else if ("vb".equals(e.getActionCommand())){
			octavaAndBassoChoice = 1;
		}
		else if ("none".equals(e.getActionCommand())){
			octavaAndBassoChoice = 2;
		}
		else if ("va".equals(e.getActionCommand())){
			octavaAndBassoChoice = 3;
		}
		else if ("ma".equals(e.getActionCommand())){
			octavaAndBassoChoice = 4;
		}
		
		else if ("sharps".equals(e.getActionCommand())){
			spellingChoice = 0;
		}
		else if ("flats".equals(e.getActionCommand())){
			spellingChoice = 1;
		}
		
		else if ("show".equals(e.getActionCommand())){
			ratioChoice = 0;
		}
		else if ("hide".equals(e.getActionCommand())){
			ratioChoice = 1;
		}
		
		for (int i = 0; i < listeningNotes.size(); i++){
			if ("bass".equals(e.getActionCommand()) 
					|| "tenor".equals(e.getActionCommand())
					|| "alto".equals(e.getActionCommand())
					|| "treble".equals(e.getActionCommand())){
				((Note) listeningNotes.get(i)).clefChoice = clefChoice;
			}
			
			if ("mb".equals(e.getActionCommand()) 
					|| "vb".equals(e.getActionCommand())
					|| "none".equals(e.getActionCommand())
					|| "va".equals(e.getActionCommand())
					|| "ma".equals(e.getActionCommand())){
				((Note) listeningNotes.get(i)).octavaAndBassoChoice = octavaAndBassoChoice;
			}
			
			if ("sharps".equals(e.getActionCommand()) 
					|| "flats".equals(e.getActionCommand())){
				((Note) listeningNotes.get(i)).spellingChoice = spellingChoice;
			}
			
			if ("show".equals(e.getActionCommand()) 
					|| "hide".equals(e.getActionCommand())){
				((Note) listeningNotes.get(i)).ratioChoice = ratioChoice;
			}
			((Note) listeningNotes.get(i)).repaint();
		}
	}
}


